project(SPINET)

find_package(KDE4 REQUIRED)

include(KDE4Defaults)

if(WIN32)
    SET(CMAKE_SHARED_LINKER_FLAGS "-Wl,--export-all-symbols -Wl,--enable-auto-import")
    SET(CMAKE_MODULE_LINKER_FLAGS "-Wl,--export-all-symbols -Wl,--enable-auto-import")
endif(WIN32)

include_directories(${SPINET_SOURCE_DIR} ${KDE4_INCLUDES} ${QT_INCLUDES})

add_subdirectory(doc)
add_subdirectory(icons)
add_subdirectory(interfaces)
add_subdirectory(shell)
add_subdirectory(app)
add_subdirectory(plugins)
