/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "midievent.h"

namespace Spinet
{

class MidiEventPrivate
{
public:
    MidiEventPrivate();

    long tick;
    IMidiEvent::Type type;
    QString errorStr;
    int param[5];
    QByteArray bytearrayData;
    QString stringData;
};

MidiEventPrivate::MidiEventPrivate()
{
    tick = 0L;
    param[0] = param[1] = param[2] = param[3] = param[4] = 0;
}

IMidiEvent::Type MidiEvent::type() const
{
    return d->type;
}

const QString &MidiEvent::errorStr() const
{
    return d->errorStr;
}

int MidiEvent::param(int index) const
{
    return d->param[index];
}

const QByteArray &MidiEvent::bytearrayData() const
{
    return d->bytearrayData;
}

const QString &MidiEvent::stringData() const
{
    return d->stringData;
}

long MidiEvent::tick() const
{
    return d->tick;
}

MidiEvent::~MidiEvent()
{
    delete d;
}

MidiEvent::MidiEvent(long tick, IMidiEvent::Type type, int param0, int param1, int param2, int param3, int param4, QObject *parent)
: IMidiEvent(parent), d(new MidiEventPrivate)
{
    d->tick = tick;
    d->type = type;
    d->param[0] = param0;
    d->param[1] = param1;
    d->param[2] = param2;
    d->param[3] = param3;
    d->param[4] = param4;
}

MidiEvent::MidiEvent(long tick, IMidiEvent::Type type, const QString &errorStr, QObject *parent)
: IMidiEvent(parent), d(new MidiEventPrivate)
{
    d->tick = tick;
    d->type = type;
    d->errorStr = errorStr;
}

MidiEvent::MidiEvent(long tick, IMidiEvent::Type type, const QByteArray &bytearrayData, QObject *parent)
: IMidiEvent(parent), d(new MidiEventPrivate)
{
    d->tick = tick;
    d->type = type;
    d->bytearrayData = bytearrayData;
}

MidiEvent::MidiEvent(long tick, IMidiEvent::Type type, int param0, const QByteArray &bytearrayData, QObject *parent)
: IMidiEvent(parent), d(new MidiEventPrivate)
{
    d->tick = tick;
    d->type = type;
    d->param[0] = param0;
    d->bytearrayData = bytearrayData;
}

MidiEvent::MidiEvent(long tick, IMidiEvent::Type type, int param0, const QString &stringData, QObject *parent)
: IMidiEvent(parent), d(new MidiEventPrivate)
{
    d->tick = tick;
    d->type = type;
    d->param[0] = param0;
    d->stringData = stringData;
}

}

