/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef SOUNDCONTROLLER_H
#define SOUNDCONTROLLER_H

#include <interfaces/isoundcontroller/isoundcontroller.h>

#include "midievent.h"
#include "../shellexport.h"

namespace Spinet
{

class Core;
class CorePrivate;
class IAudioSequencer;
class IMidiSequencer;

class SPINETSHELL_EXPORT SoundController : public ISoundController
{
    Q_OBJECT
    friend class CorePrivate;
public:
    SoundController(Core *core);
    virtual ~SoundController();

    virtual const QList<IAudioSequencer *> &audioSequencers() const;
    virtual const QList<IMidiSequencer *>  &midiSequencers()  const;
    virtual const QList<IMidiDevice *>     &midiDevices()     const;

    virtual IMidiObject *newMidiObject(QObject *parent = 0) const;

    virtual IMidiEvent *newMidiEvent(long tick, IMidiEvent::Type type, int param0 = 0, int param1 = 0, int param2 = 0, int param3 = 0, int param4 = 0, QObject *parent = 0) const;
    virtual IMidiEvent *newMidiEvent(long tick, IMidiEvent::Type type, const QString &errorStr, QObject *parent = 0) const;
    virtual IMidiEvent *newMidiEvent(long tick, IMidiEvent::Type type, const QByteArray &bytearrayData, QObject *parent = 0) const;
    virtual IMidiEvent *newMidiEvent(long tick, IMidiEvent::Type type, int param0, const QByteArray &bytearrayData, QObject *parent = 0) const;
    virtual IMidiEvent *newMidiEvent(long tick, IMidiEvent::Type type, int param0, const QString &stringData, QObject *parent = 0) const;

    void addAudioSequencer(IAudioSequencer *audioSequencer);
    void addMidiSequencer (IMidiSequencer  *midiSequencer);
    void addMidiDevice    (IMidiDevice     *midiDevice);

private:
    class SoundControllerPrivate *const d;
};

}

#endif
