/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef MIDIOBJECT_H
#define MIDIOBJECT_H

#include <interfaces/isoundcontroller/imidiobject.h>

#include "../shellexport.h"

namespace Spinet
{

class IMidiEvent;

class SPINETSHELL_EXPORT MidiObject : public IMidiObject
{
    Q_OBJECT
public:
    MidiObject(QObject *parent = 0);
    virtual ~MidiObject();

    virtual void appendEvent(IMidiEvent *event);

    Q_INVOKABLE virtual bool hasNext();
    Q_INVOKABLE virtual IMidiEvent *nextEvent();

    virtual void resetPosition();
    virtual void clear();
    virtual void sort();

private:
    class MidiObjectPrivate *const d;
};

}

#endif
