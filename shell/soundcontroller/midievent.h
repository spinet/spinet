/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef MIDIEVENT_H
#define MIDIEVENT_H

#include <interfaces/isoundcontroller/imidievent.h>

#include "../shellexport.h"

class QString;
class QByteArray;

namespace Spinet
{

class SPINETSHELL_EXPORT MidiEvent : public IMidiEvent
{
    Q_OBJECT
public:
    virtual Type type() const;
    virtual const QString &errorStr() const;
    virtual int param(int index) const;
    virtual const QByteArray &bytearrayData() const;
    virtual const QString &stringData() const;
    virtual long tick() const;

    virtual ~MidiEvent();

    MidiEvent(long tick, IMidiEvent::Type type, int param0 = 0, int param1 = 0, int param2 = 0, int param3 = 0, int param4 = 0, QObject *parent = 0);
    MidiEvent(long tick, IMidiEvent::Type type, const QString &errorStr, QObject *parent = 0);
    MidiEvent(long tick, IMidiEvent::Type type, const QByteArray &bytearrayData, QObject *parent = 0);
    MidiEvent(long tick, IMidiEvent::Type type, int param0, const QByteArray &bytearrayData, QObject *parent = 0);
    MidiEvent(long tick, IMidiEvent::Type type, int param0, const QString &stringData, QObject *parent = 0);

private:
    class MidiEventPrivate *const d;
};

}

#endif
