/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "soundcontroller.h"

#include <KDebug>

#include "midiobject.h"
#include "midievent.h"

namespace Spinet
{

class SoundControllerPrivate
{
public:
    SoundControllerPrivate(Core *core);
    ~SoundControllerPrivate();

    QList<IAudioSequencer *> audioSequencers;
    QList<IMidiSequencer  *> midiSequencers;
    QList<IMidiDevice     *> midiDevices;

    Core *m_core;
};

SoundControllerPrivate::SoundControllerPrivate(Core *core)
: m_core (core)
{
}

SoundControllerPrivate::~SoundControllerPrivate()
{
}

SoundController::SoundController(Core *core)
: ISoundController(), d(new SoundControllerPrivate(core))
{
}

SoundController::~SoundController()
{
    delete d;
}

const QList<IAudioSequencer *> &SoundController::audioSequencers() const
{
    return d->audioSequencers;
}

const QList<IMidiSequencer *> &SoundController::midiSequencers() const
{
    return d->midiSequencers;
}

const QList<IMidiDevice *> &SoundController::midiDevices() const
{
    return d->midiDevices;
}

IMidiObject *SoundController::newMidiObject(QObject *parent) const
{
    return new MidiObject(parent);
}

IMidiEvent *SoundController::newMidiEvent(long tick, IMidiEvent::Type type, int param0, int param1, int param2, int param3, int param4, QObject *parent) const
{
    return new MidiEvent(tick, type, param0, param1, param2, param3, param4, parent);
}

IMidiEvent *SoundController::newMidiEvent(long tick, IMidiEvent::Type type, const QString &errorStr, QObject *parent) const
{
    return new MidiEvent(tick, type, errorStr, parent);
}

IMidiEvent *SoundController::newMidiEvent(long tick, IMidiEvent::Type type, const QByteArray &bytearrayData, QObject *parent) const
{
    return new MidiEvent(tick, type, bytearrayData, parent);
}

IMidiEvent *SoundController::newMidiEvent(long tick, IMidiEvent::Type type, int param0, const QByteArray &bytearrayData, QObject *parent) const
{
    return new MidiEvent(tick, type, param0, bytearrayData, parent);
}

IMidiEvent *SoundController::newMidiEvent(long tick, IMidiEvent::Type type, int param0, const QString &stringData, QObject *parent) const
{
    return new MidiEvent(tick, type, param0, stringData, parent);
}

void SoundController::addAudioSequencer(IAudioSequencer *audioSequencer)
{
    d->audioSequencers.append(audioSequencer);
}

void SoundController::addMidiSequencer(IMidiSequencer *midiSequencer)
{
    d->midiSequencers.append(midiSequencer);
}

void SoundController::addMidiDevice (IMidiDevice *midiDevice)
{
    d->midiDevices.append(midiDevice);
}

}

