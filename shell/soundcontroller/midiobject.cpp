/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/
#include "midiobject.h"

#include <interfaces/isoundcontroller/imidievent.h>

#include <QtCore/QDebug>

namespace Spinet
{

class MidiObjectPrivate
{
public:
    MidiObjectPrivate();

    QList<IMidiEvent *> midiEvents;
    QList<IMidiEvent *>::const_iterator i;
};

MidiObjectPrivate::MidiObjectPrivate()
{
    i = midiEvents.constBegin();
}

static inline bool eventLessThan(const IMidiEvent *e1, const IMidiEvent *e2)
{
    return e1->tick() < e2->tick();
}

MidiObject::MidiObject(QObject *parent)
: IMidiObject(parent), d(new MidiObjectPrivate)
{
    resetPosition();
}

MidiObject::~MidiObject()
{
    delete d;
}

void MidiObject::appendEvent(IMidiEvent *event)
{
    d->midiEvents.append(event);
}

bool MidiObject::hasNext()
{
    return d->i != d->midiEvents.constEnd();
}

IMidiEvent *MidiObject::nextEvent()
{
    IMidiEvent *event = *(d->i);
    (d->i)++;
    return event;
}

void MidiObject::resetPosition()
{
   d->i = d->midiEvents.constBegin();
}

void MidiObject::clear()
{
    d->midiEvents.clear();
    resetPosition();
}

void MidiObject::sort()
{
    qStableSort(d->midiEvents.begin(), d->midiEvents.end(), eventLessThan);
}

}

