/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef CORE_H
#define CORE_H

#include <interfaces/icore.h>

#include "shellexport.h"

namespace Spinet
{

class UiController;
class PluginController;
class SoundController;

class SPINETSHELL_EXPORT Core: public ICore // krazy:exclude=dpointer
{
public:
    static bool initialize();
    virtual ~Core();

    static Core *self();

    virtual IUiController     *uiController();
    virtual IPluginController *pluginController();
    virtual ISoundController  *soundController();

    UiController     *uiControllerInternal();
    PluginController *pluginControllerInternal();
    SoundController  *soundControllerInternal();

    void cleanup();

protected:
    class CorePrivate *const d;
    static Core *m_self;

private:
    Core(QObject *parent = 0);
};

}

#endif
