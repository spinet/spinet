/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "uicontroller.h"

#include <QtGui/QDockWidget>
#include <QtCore/QPointer>
#include <QtCore/QSettings>
#include <QWidget>

#include <KConfig>
#include <KConfigGroup>
#include <KDE/KSettings/Dialog>
#include <KDebug>

#include "mainwindow.h"

namespace Spinet
{

class UiControllerPrivate
{
public:
    Core *core;
    QPointer<MainWindow> mainWindow;
    QPointer<KSettings::Dialog> settingsDialog;
    
    KConfig config;
};

UiController::UiController(Core *core)
: IUiController(), d(new UiControllerPrivate)
{
    kDebug();
    d->core = core;
}

UiController::~UiController()
{
     /*QList<QDockWidget *> dockWidgets = d->mainWindow->findChildren<QDockWidget *>();
    foreach(QDockWidget *dockWidget, dockWidgets)
	writeWidgetSettings(dockWidget->widget());
    writeWidgetSettings(d->mainWindow->centralWidget());*/
    delete d;
}

void UiController::setCentralWidget(QWidget *centralWidget)
{
    readWidgetSettings(centralWidget);
    centralWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    d->mainWindow->setCentralWidget(centralWidget);
}

void UiController::addDockWidget(const QString &title, QWidget *widget, Qt::DockWidgetArea dockArea)
{
    readWidgetSettings(widget);
    QDockWidget *dockWidget = new QDockWidget(title, d->mainWindow);
    dockWidget->setWidget(widget);
    dockWidget->setAllowedAreas(Qt::AllDockWidgetAreas);
    widget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    //dockWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    dockWidget->setObjectName(title);
    d->mainWindow->addDockWidget(dockArea, dockWidget);
}

void UiController::showSettingsDialog()
{
    KSettings::Dialog configDialog(QStringList() << "spinet", d->mainWindow);
    configDialog.exec();	// krazy:exclude=crashy
    emit settingsChanged();
}

void UiController::loadSettings()
{
    emit settingsChanged();
}

void UiController::initialize()
{
    kDebug();
    d->mainWindow = new MainWindow;
    d->mainWindow->initialize();
}

void UiController::writeWidgetSettings(QWidget *widget)
{
    KConfigGroup widgetGroup(&d->config, "Spinet");

    widgetGroup.writeEntry(widget->objectName() + "Size", widget->size());
    widgetGroup.writeEntry(widget->objectName() + "Point", widget->pos());
    widgetGroup.config()->sync();
    
    kDebug() << "Escrevendo: " << widget->objectName() << "| Size: " << widget->size() << " | Point: " << widget->pos();
}

void UiController::readWidgetSettings(QWidget *widget)
{
    KConfigGroup widgetGroup(&d->config, "Spinet");

    QSize size = widgetGroup.readEntry(widget->objectName() + "Size", QSize());
    QPoint pos = widgetGroup.readEntry(widget->objectName() + "Point", QPoint());

    widget->resize(size);
    widget->move(pos);
    kDebug() << "Lendo: " << widget->objectName() << "| Size: " << size << " | Point: " << pos;
}

}
