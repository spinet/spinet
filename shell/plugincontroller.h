/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef PLUGINCONTROLLER_H
#define PLUGINCONTROLLER_H

#include <KDE/KService>
#include <KDE/KPluginInfo>
#include <KDE/KServiceTypeTrader>

#include <interfaces/iplugincontroller.h>

#include "shellexport.h"

namespace Spinet
{

class Core;
class IPlugin;
class CorePrivate;

class SPINETSHELL_EXPORT PluginController : public IPluginController
{
    Q_OBJECT

    friend class CorePrivate;
public:
    PluginController(Core *core);
    virtual ~PluginController();

    // Methods from IPluginController
    virtual KPluginInfo pluginInfo(const IPlugin *plugin) const;
    virtual QList<IPlugin *> loadedPlugins() const;
    virtual IPlugin *loadPlugin(const QString &pluginName);
    virtual bool unloadPlugin(const QString &pluginName);

    // Additional methods
    IPlugin* plugin(const QString &pluginName);
    QList<KPluginInfo> allPluginInfos() const;
    void updateLoadedPlugins();
    void resetToDefaults();
    void cleanup();
    
    IPlugin *pluginForExtension(const QString &extension, const QString &pluginName = "");
    IPlugin *pluginForExtension(const QString &extension, const QStringList &constraints);
    
    KPluginInfo infoForPluginId(const QString &pluginId) const;

private:
    IPlugin* loadPluginInternal(const QString &pluginId);
    void initialize();
    KPluginInfo infoForPluginName(const QString &pluginName) const;
    bool isEnabled(const KPluginInfo &info);
    bool checkForDependencies(const KPluginInfo &info, QStringList &missing) const;
    bool loadDependencies(const KPluginInfo &info, QString &failedPlugin);
    void loadOptionalDependencies(const KPluginInfo &info);

    class PluginControllerPrivate *const d;
};

}

#endif
