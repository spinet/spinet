/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "plugincontroller.h"

#include <QtCore/QSettings>

#include <QtGui/QWidget>

#include <KDebug>
#include <KServiceTypeTrader>

#include <interfaces/icore.h>
#include <interfaces/iplugin.h>
#include <interfaces/isoundcontroller/iaudiosequencer.h>
#include <interfaces/isoundcontroller/imidisequencer.h>

#include "core.h"
#include "uicontroller.h"
#include "soundcontroller/soundcontroller.h"

namespace Spinet
{

static const QString pluginControllerGrp("Plugins");

bool isUserSelectable(const KPluginInfo &info)
{
    QString loadMode = info.property("X-Spinet-LoadMode").toString();
    return loadMode.isEmpty() || loadMode == "UserSelectable";
}

bool isGlobalPlugin(const KPluginInfo &info)
{
    return info.property("X-Spinet-Category").toString() == "Global";
}

bool hasMandatoryProperties(const KPluginInfo &info)
{
    QVariant mode = info.property("X-Spinet-Mode");
    QVariant version = info.property("X-Spinet-Version");

    return mode.isValid() && mode.canConvert(QVariant::String)
           && version.isValid() && version.canConvert(QVariant::String);
}

class PluginControllerPrivate
{
public:
    QList<KPluginInfo> plugins;
    typedef QMap<KPluginInfo, IPlugin*> InfoToPluginMap;
    InfoToPluginMap loadedPlugins;

    Core *core;
};

PluginController::PluginController(Core *core)
: IPluginController(), d(new PluginControllerPrivate)
{
  kDebug();
    d->core = core;
    d->plugins = KPluginInfo::fromServices(KServiceTypeTrader::self()->query(QLatin1String("Spinet/Plugin"),
                 QString("[X-Spinet-Version] == %1").arg(SPINET_PLUGIN_VERSION)));
    kDebug() << "Found" << d->plugins.size() << "plugins";
}

PluginController::~PluginController()
{
  kDebug();
    delete d;
}

KPluginInfo PluginController::pluginInfo(const IPlugin *plugin) const
{
    for (PluginControllerPrivate::InfoToPluginMap::ConstIterator it = d->loadedPlugins.constBegin();
         it != d->loadedPlugins.constEnd();
         ++it)
    {
        if (it.value() == plugin)
            return it.key();
    }
    return KPluginInfo();
}

QList<IPlugin *> PluginController::loadedPlugins() const
{
    return d->loadedPlugins.values();
}

IPlugin *PluginController::loadPlugin(const QString &pluginName)
{
    KPluginInfo info = infoForPluginName(pluginName);
    if (!info.isValid())
    {
        kWarning() << "Unable to find a plugin named '" << pluginName << "'!";
        return 0;
    }

    if (d->loadedPlugins.contains(info))
        return d->loadedPlugins[info];

    if(!isEnabled(info))
    {
        // Do not load disabled plugins
        kWarning() << "Not loading plugin named" << pluginName << "because its been disabled!";
        return 0;
    }

    if(!hasMandatoryProperties(info))
    {
        kWarning() << "Unable to load plugin named " << pluginName << "! Doesn't have all mandatory properties set";
        return 0;
    }

    emit loadingPlugin(info.pluginName());

    QString str_error;
    IPlugin *plugin = 0;
    QStringList missingInterfaces;
    
    if (checkForDependencies(info, missingInterfaces))
    {
        QString failedPlugin;
        if(!loadDependencies(info, failedPlugin))
        {
            kWarning() << "Could not load a required dependency: " << failedPlugin;
            return 0;
        }
        loadOptionalDependencies(info);

	plugin = KServiceTypeTrader::createInstanceFromQuery<IPlugin>(QLatin1String("Spinet/Plugin"),
		 QString::fromLatin1("[X-KDE-PluginInfo-Name]=='%1'").arg(pluginName),
				     d->core, QVariantList(), &str_error);

	if (plugin)
	{
	    d->loadedPlugins.insert(info, plugin);
	    info.setPluginEnabled(true);
	    kDebug() << "Successfully loaded plugin '" << pluginName << "'";
	    emit pluginLoaded(plugin, info);

	    QVariant prop = info.property("X-Spinet-Interfaces");
            if(prop.canConvert<QStringList>())
	    {
	        QStringList interfaces = prop.toStringList();
		if (interfaces.contains("org.liveblue.IAudioSequencer"))
		    Core::self()->soundControllerInternal()->addAudioSequencer(dynamic_cast<IAudioSequencer *>(plugin));
		if (interfaces.contains("org.liveblue.IMidiSequencer"))
		    Core::self()->soundControllerInternal()->addMidiSequencer(dynamic_cast<IMidiSequencer *>(plugin));
		if (interfaces.contains("org.liveblue.IMidiDevice"))
		    Core::self()->soundControllerInternal()->addMidiDevice(dynamic_cast<IMidiDevice *>(plugin));
	    }
	}
	else
	{
	    kWarning() << "Loading plugin '" << pluginName
		<< "' failed, KPluginLoader reported error: '" << endl <<
		str_error << "'";
	}
    }
    else
    {
	kWarning() << "Loading plugin '" << pluginName
	    << "' failed, missing dependencies: '" << missingInterfaces;
    }
    
    return plugin;
}

bool PluginController::unloadPlugin(const QString &pluginName)
{
    IPlugin *thePlugin = plugin(pluginName);
    if (thePlugin)
    {
        thePlugin->unload();
        emit pluginUnloaded(thePlugin, infoForPluginName(pluginName));
        for (PluginControllerPrivate::InfoToPluginMap::Iterator it = d->loadedPlugins.begin();
             it != d->loadedPlugins.end(); ++it)
        {
            if (it.value() == thePlugin)
            {
                d->loadedPlugins.erase(it);
                break;
            }
        }
        return true;
    }
    return false;
}

IPlugin* PluginController::plugin(const QString &pluginName)
{
    KPluginInfo info = infoForPluginName(pluginName);
    if (!info.isValid())
        return 0;

    if (d->loadedPlugins.contains(info))
        return d->loadedPlugins[info];
    else
        return 0;
}

QList<KPluginInfo> PluginController::allPluginInfos() const
{
    return d->plugins;
}

void PluginController::updateLoadedPlugins()
{
    KConfigGroup grp = KGlobal::config()->group(pluginControllerGrp);
    foreach(const KPluginInfo &info, d->plugins)
    {
        if(isGlobalPlugin(info))
        {
            bool enabled = grp.readEntry(info.pluginName() + "Enabled", true) || !isUserSelectable(info);
            if(d->loadedPlugins.contains(info) && !enabled)
            {
                kDebug() << "unloading" << info.pluginName();
                if(!unloadPlugin(info.pluginName()))
                {
                    grp.writeEntry(info.pluginName()+"Enabled", false);
                }
            } else if(!d->loadedPlugins.contains(info) && enabled)
            {
                loadPlugin(info.pluginName());
            }
        }
    }
}

void PluginController::resetToDefaults()
{
    KSharedConfig::Ptr cfg = KGlobal::config();
    cfg->deleteGroup(pluginControllerGrp);
    cfg->sync();
    KConfigGroup grp = cfg->group(pluginControllerGrp);
    QStringList plugins;
    foreach(const KPluginInfo& info, d->plugins)
    {
	plugins << info.pluginName();
    }
    foreach(const QString& s, plugins)
    {
        grp.writeEntry(s + "Enabled", true);
    }
    grp.sync();
}

void PluginController::cleanup()
{
    // Ask all plugins to unload
    while (!d->loadedPlugins.isEmpty())
    {
        // Let the plugin do some stuff before unloading
        unloadPlugin(d->loadedPlugins.begin().key().pluginName());
    }
}


IPlugin *PluginController::pluginForExtension( const QString& extension, const QString& pluginName)	
{
    QStringList constraints;
    
    if(!pluginName.isEmpty())
	constraints << QString("[X-KDE-PluginInfo-Name]=='%1'").arg(pluginName);
    
    return pluginForExtension(extension, constraints);
}

IPlugin *PluginController::pluginForExtension(const QString &extension, const QStringList &constraints)	
{
    KPluginInfo::List info = queryExtensionPlugins(extension, constraints);
    
    if(info.isEmpty())
	return 0;
    
    if(d->loadedPlugins.contains(info.first()))
	return d->loadedPlugins[info.first()];
    else
	return loadPluginInternal(info.first().pluginName());
}


IPlugin *PluginController::loadPluginInternal(const QString &pluginId)
{
    KPluginInfo info = infoForPluginId(pluginId);
    
    if(!info.isValid())
    {
	kWarning(9501) << "Unable to find a plugin named '" << pluginId << "'!" ;
        return 0L;
    }
    
    if(d->loadedPlugins.contains(info))
	return d->loadedPlugins[info];
    
    if(!isEnabled(info))	
    {	
        kWarning() << "Not loading plugin named" << pluginId << "because its been disabled!";
        return 0;
    }
    
    if(!hasMandatoryProperties(info)) 
    {
        kWarning() << "Unable to load plugin named " << pluginId << "! Doesn't have all mandatory properties set";
        return 0;
    }
    
    if( info.property("X-Spinet-Mode") == "GUI")
    {
        kDebug() << "Unable to load plugin named" << pluginId << ". Running in No-Ui mode, but the plugin says it needs a GUI";
        return 0;
    }
    
    bool isKrossPlugin = false;
    
    kDebug() << "Attempting to load '" << pluginId << "'";
    emit loadingPlugin(info.pluginName());
    QString str_error;
    IPlugin *plugin = 0;
    QStringList missingInterfaces;
    kDebug() << "Checking... " << info.name();
    
    if(checkForDependencies(info, missingInterfaces))	
    {
        QVariant prop = info.property("X-Spinet-PluginType");
        kDebug() << "Checked... starting to load:" << info.name() << "type:" << prop;
	
        QString failedPlugin;
	
        if(!loadDependencies(info, failedPlugin))
        {
            kWarning() << "Could not load a required dependency:" << failedPlugin;
            return 0;
        }
        
        loadOptionalDependencies(info);
	
        if(isKrossPlugin)
        {
        }
        else
	{
            plugin = KServiceTypeTrader::createInstanceFromQuery<IPlugin>(QLatin1String("Spinet/Plugin" ),
                    QString::fromLatin1("[X-KDE-PluginInfo-Name]=='%1'").arg(pluginId), 
					d->core, QVariantList(), &str_error);
        }
    }
    
    if(plugin)
    {
	d->loadedPlugins.insert(info, plugin);
        info.setPluginEnabled(true);
	
	kDebug() << "Successfully loaded plugin '" << pluginId << "'";
	
        emit pluginLoaded(plugin, info);
    }
    else
    {
        if(str_error.isEmpty() && !missingInterfaces.isEmpty())
        {
            kWarning() << "Can't load plugin '" << pluginId
                    << "' some of its required dependencies could not be fulfilled:" << endl
                    << missingInterfaces.join(",") << endl;
        }
        else
        {
            kWarning() << "Loading plugin '" << pluginId << "' failed, KPluginLoader reported error: '" 
		       << endl << str_error << "'";
        }
    }
    
    return plugin;
}


KPluginInfo PluginController::infoForPluginId(const QString &pluginId) const
{
    QList<KPluginInfo>::ConstIterator it;
    
    for(it = d->plugins.constBegin(); it != d->plugins.constEnd(); ++it)
    {
	if(it->pluginName() == pluginId)
	    return *it;
    }
    
    return KPluginInfo();
}
    
void PluginController::initialize()
{
    QMap<QString, bool> pluginMap;
    foreach(const KPluginInfo& pi, d->plugins)
    {
	pluginMap.insert(pi.pluginName(), true);
    }

    KConfigGroup grp = KGlobal::config()->group(pluginControllerGrp);
    QMap<QString, QString> entries = grp.entryMap();

    QMap<QString, QString>::Iterator it;
    for (it = entries.begin(); it != entries.end(); ++it)
    {
        QString key = it.key();
        if (key.endsWith(QLatin1String("Enabled")))
        {
            QString pluginName = key.left(key.length() - 7);
            bool defValue;
            QMap<QString, bool>::const_iterator entry = pluginMap.constFind(pluginName);
            if(entry != pluginMap.constEnd())
            {
                defValue = entry.value();
            } else {
                defValue = false;
            }
            pluginMap.insert(key.left(key.length() - 7), grp.readEntry(key, defValue));
        }
    }

    foreach(const KPluginInfo &pi, d->plugins)
    {
        if(isGlobalPlugin(pi))
        {
            QMap<QString, bool>::const_iterator it = pluginMap.constFind(pi.pluginName());
            if(it != pluginMap.constEnd() && (it.value() || !isUserSelectable(pi)))
            {
                // Plugin is mentioned in pluginmap and the value is true, so try to load it
                loadPlugin(pi.pluginName());
                if(!grp.hasKey(pi.pluginName() + "Enabled"))
                {
                    if(isUserSelectable(pi))
                    {
                        // If plugin isn't listed yet, add it with true now
                        grp.writeEntry(pi.pluginName() + "Enabled", true);
                    }
                } else if(grp.hasKey(pi.pluginName() + "Disabled") && !isUserSelectable(pi))
                {
                    // Remove now-obsolete entries
                    grp.deleteEntry(pi.pluginName() + "Disabled");
                }
            }
        }
        else
	    kDebug() << pi.pluginName() << "isn't a global plugin and won't be loaded by now";
    }
    // Synchronize so we're writing out to the file
    grp.sync();

    kDebug() << "We have " << Core::self()->soundController()->audioSequencers().size() << " audio sequencer(s)";
    kDebug() << "We have " << Core::self()->soundController()->midiSequencers().size() << " midi sequencer(s)";
    kDebug() << "We have " << Core::self()->soundController()->midiDevices().size() << " midi device(s)";
}

KPluginInfo PluginController::infoForPluginName(const QString &pluginName) const
{
    QList<KPluginInfo>::ConstIterator it;
    for (it = d->plugins.constBegin(); it != d->plugins.constEnd(); ++it)
    {
        if (it->pluginName() == pluginName)
            return *it;
    }
    return KPluginInfo();
}

bool PluginController::isEnabled(const KPluginInfo &info)
{
    Q_UNUSED(info);
    return true;
}

bool PluginController::checkForDependencies(const KPluginInfo &info, QStringList &missing) const
{
    QVariant prop = info.property("X-Spinet-IRequired");
    bool result = true;
    if(prop.canConvert<QStringList>())
    {
        QStringList deps = prop.toStringList();
        foreach(const QString &iface, deps)
        {
            KPluginInfo::List l = queryPlugins(QString("'%1' in [X-Spinet-Interfaces]").arg(iface));
            if(l.isEmpty())
            {
                result = false;
                missing << iface;
            }
        }
    }
    return result;
}

bool PluginController::loadDependencies(const KPluginInfo &info, QString &failedPlugin)
{
    QVariant prop = info.property("X-Spinet-IRequired");
    QStringList loadedPlugins;
    if(prop.canConvert<QStringList>())
    {
        QStringList deps = prop.toStringList();
        foreach(const QString &iface, deps)
        {
            KPluginInfo info = queryPlugins(QString("'%1' in [X-Spinet-Interfaces]").arg(iface)).first();
            if(!loadPlugin(info.pluginName()))
            {
                foreach(const QString &name, loadedPlugins)
                {
                    unloadPlugin(name);
                }
                failedPlugin = info.pluginName();
                return false;
            }
            loadedPlugins << info.pluginName();
        }
    }
    return true;
}

void PluginController::loadOptionalDependencies(const KPluginInfo &info)
{
    QVariant prop = info.property("X-Spinet-IOptional");
    if(prop.canConvert<QStringList>())
    {
        QStringList deps = prop.toStringList();
        foreach(const QString &iface, deps)
        {
            KPluginInfo info = queryPlugins(QString("'%1' in [X-Spinet-Interfaces]").arg(iface)).first();
            if(!loadPlugin(info.pluginName()))
	    {
		kDebug() << "Couldn't load optional dependency: " << iface << info.pluginName();
	    }
        }
    }
}

}
