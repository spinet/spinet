/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "kcmpluginsettings.h"

#include <QVBoxLayout>

#include <KGenericFactory>
#include <KAboutData>
#include <KPluginSelector>
#include <KPluginInfo>
#include <KSettings/Dispatcher>

#include "../core.h"
#include "../plugincontroller.h"

namespace Spinet
{

K_PLUGIN_FACTORY(KcmPluginSettingsFactory, registerPlugin<KcmPluginSettings>();)
K_EXPORT_PLUGIN(KcmPluginSettingsFactory(KAboutData("kcm_spinet_pluginsettings", "spinet",
                                         ki18n("Plugin Settings"), "1.0")))

KcmPluginSettings::KcmPluginSettings(QWidget *parent, const QVariantList &args)
: KCModule(KcmPluginSettingsFactory::componentData(), parent, args)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    selector = new KPluginSelector(this);
    layout->addWidget(selector);
    
    QMap<QString, QList<KPluginInfo> > plugins;
    QMap<QString, QString> categories;
    categories["Core"] = i18n("Core");
    categories["Audio Sequencers"] = i18n("Audio Sequencers");
    categories["MIDI Sequencers"] = i18n("MIDI Sequencers");
    categories["MIDI Devices"] = i18n("MIDI Devices");
    categories["MIDI Object Factories"] = i18n("MIDI Object Factories");

    foreach(const KPluginInfo &info, Core::self()->pluginControllerInternal()->allPluginInfos())
    {
        QString loadMode = info.property("X-Spinet-LoadMode").toString();
        if((loadMode.isEmpty() || loadMode == "UserSelectable"))
        {
            QString category = info.category();
            if (!categories.contains(category)) {
                if (!category.isEmpty()) {
                    kWarning() << "unknown category for plugin" << info.name() << ":" << info.category();
                }
                category = "Other";
            }
            plugins[category] << info;
        }
    }
    QMap< QString, QList<KPluginInfo> >::const_iterator it = plugins.constBegin();
    QMap< QString, QList<KPluginInfo> >::const_iterator end = plugins.constEnd();
    while(it != end)
    {
        selector->addPlugins(it.value(), KPluginSelector::ReadConfigFile,
                             categories.value(it.key()), it.key());
        ++it;
    }

    connect(selector, SIGNAL(changed(bool)), this, SLOT(changed()));
    connect(selector, SIGNAL(configCommitted(QByteArray)), this, SLOT(reparseConfig(QByteArray)));
    selector->load();
}

void KcmPluginSettings::reparseConfig(const QByteArray &conf)
{
    KSettings::Dispatcher::reparseConfiguration(conf);
}

void KcmPluginSettings::load()
{
    kDebug() << "Save";
    selector->load();
    KCModule::load();
}

void KcmPluginSettings::save()
{
    kDebug() << "Save";
    selector->save();
    KCModule::save();
    Core::self()->pluginControllerInternal()->updateLoadedPlugins();
    selector->load(); // Some plugins may have failed to load, they must be unchecked.
}

void KcmPluginSettings::defaults()
{
    Core::self()->pluginControllerInternal()->resetToDefaults();
    selector->load();
    KCModule::defaults();
}

}

