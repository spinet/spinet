/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "core.h"

#include "plugincontroller.h"
#include "uicontroller.h"
#include "soundcontroller/soundcontroller.h"

#include <KDebug>

namespace Spinet
{
class CorePrivate
{
public:
    CorePrivate(Core *core);
    ~CorePrivate();

    bool initialize();

    QPointer<UiController>     uiController;
    QPointer<PluginController> pluginController;
    QPointer<SoundController>  soundController;

    Core *m_core;
};

CorePrivate::CorePrivate(Core *core)
 : m_core(core)
{
}

bool CorePrivate::initialize()
{
    kDebug();
    if(!pluginController)
    {
        pluginController = new PluginController(m_core);
    }
    if(!uiController)
    {
        uiController = new UiController(m_core);
    }
    if(!soundController)
    {
        soundController = new SoundController(m_core);
    }
    uiController->initialize();
    pluginController->initialize();

    return true;
}

CorePrivate::~CorePrivate()
{
    kDebug();
    delete uiController;
    delete pluginController;
    delete soundController;
}

Core *Core::m_self = 0;

bool Core::initialize()
{
    kDebug();
    if (m_self)
        return true;

    m_self = new Core();
    bool ret = m_self->d->initialize();

    return ret;
}

Core *Core::self()
{
    return m_self;
}

Core::Core(QObject *parent)
 : ICore(parent), d(new CorePrivate(this))
{
    kDebug();
}

Core::~Core()
{
    kDebug();
    cleanup();
    delete d;
}

IPluginController *Core::pluginController()
{
    return d->pluginController;
}

IUiController *Core::uiController()
{
    return d->uiController;
}

ISoundController *Core::soundController()
{
    return d->soundController;
}

UiController *Core::uiControllerInternal()
{
    return d->uiController;
}

PluginController *Core::pluginControllerInternal()
{
    return d->pluginController;
}

SoundController *Core::soundControllerInternal()
{
    return d->soundController;
}

void Core::cleanup()
{
    d->pluginController->cleanup();
}

}
