/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "mainwindow_p.h"

#include <QtGui/QApplication>

#include <KDE/KPluginInfo>
#include <KDE/KXMLGUIClient>
#include <KDE/KXMLGUIFactory>
#include <KDE/KStandardAction>
#include <KDE/KActionCollection>

#include "interfaces/iplugin.h"
#include "core.h"
#include "mainwindow.h"
#include "uicontroller.h"

#include <KDebug>

namespace Spinet
{
  
MainWindowPrivate::MainWindowPrivate(MainWindow *mainWindow)
: m_mainWindow(mainWindow)
{
}

void MainWindowPrivate::setupActions()
{
    KStandardAction::quit(qApp, SLOT(closeAllWindows()), actionCollection());
    KStandardAction::preferences(this, SLOT(settingsDialog()), actionCollection());
}

void MainWindowPrivate::setupGui()
{
}

void MainWindowPrivate::addPlugin(IPlugin *plugin, const KPluginInfo &pluginInfo)
{
    Q_ASSERT(plugin);
    if (pluginInfo.property("X-Spinet-Mode").toString() == "GUI")
	m_mainWindow->guiFactory()->addClient(plugin);
    else kDebug() << "NoGui plugin";
}

void MainWindowPrivate::removePlugin(IPlugin *plugin, const KPluginInfo &pluginInfo)
{
    Q_ASSERT(plugin);
    if (pluginInfo.property("X-Spinet-Mode").toString() == "GUI")
	m_mainWindow->guiFactory()->removeClient(plugin);
    else kDebug() << "NoGui plugin";
}

void MainWindowPrivate::settingsDialog()
{
    Core::self()->uiControllerInternal()->showSettingsDialog();
}

KActionCollection *MainWindowPrivate::actionCollection()
{
    return m_mainWindow->actionCollection();
}

}

