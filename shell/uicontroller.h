/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef UICONTROLLER_H
#define UICONTROLLER_H

#include <interfaces/iuicontroller.h>

#include "shellexport.h"

namespace Spinet
{

class Core;
class CorePrivate;

class SPINETSHELL_EXPORT UiController : public IUiController
{
    Q_OBJECT

    friend class CorePrivate;
public:
    UiController(Core *core);
    virtual ~UiController();

    virtual void setCentralWidget(QWidget *centralWidget);
    virtual void addDockWidget(const QString &title, QWidget *widget, Qt::DockWidgetArea dockArea);

    void showSettingsDialog();
    void loadSettings();
    
    void writeWidgetSettings(QWidget *widget);
    void readWidgetSettings(QWidget *widget);
    
private:
    void initialize();

    class UiControllerPrivate *const d;
};

}

#endif
