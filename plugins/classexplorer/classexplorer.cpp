/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/ 

#include "classexplorer.h"

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>

namespace Spinet
{

K_PLUGIN_FACTORY(ClassExplorerFactory, registerPlugin<ClassExplorer>();)
K_EXPORT_PLUGIN(ClassExplorerFactory(KAboutData("classexplorer","classexplorer",
                                  ki18n("Spinet classexplorer GUI plugin"), "1.0",
                                  ki18n("Spinet classexplorer GUI plugin"),
                                  KAboutData::License_GPL)))

ClassExplorer::ClassExplorer(QObject *parent, const QVariantList &args)
: IPlugin(ClassExplorerFactory::componentData(), parent)
{
    Q_UNUSED(args);
    
    m_view = new QTreeView;
    m_view->setObjectName("classExplorer");
    
    ICore::self()->uiController()->addDockWidget("Class Explorer", m_view, Qt::LeftDockWidgetArea);
}


ClassExplorer::~ClassExplorer()
{
}

void ClassExplorer::unload()
{
}
  
}
