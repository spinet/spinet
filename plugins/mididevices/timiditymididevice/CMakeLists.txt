add_subdirectory(icons)
add_subdirectory(settings)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

set(timiditymididevice_SRCS
    timiditymididevice.cpp
)

kde4_add_kcfg_files(timiditymididevice_SRCS settings/spinettimiditymididevicesettings.kcfgc)

kde4_add_plugin(timiditymididevice ${timiditymididevice_SRCS})
target_link_libraries(timiditymididevice ${KDE4_KDEUI_LIBS} spinetinterfaces)

install(TARGETS timiditymididevice DESTINATION ${PLUGIN_INSTALL_DIR})

install(FILES spinettimiditymididevice.desktop DESTINATION ${SERVICES_INSTALL_DIR})
