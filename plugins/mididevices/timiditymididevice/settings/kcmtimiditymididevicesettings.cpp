/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "kcmtimiditymididevicesettings.h"

#include <QtGui/QVBoxLayout>

#include <KDE/KAboutData>
#include <KDE/KUrlRequester>
#include <KDE/KGenericFactory>

#include "spinettimiditymididevicesettings.h"
#include "ui_spinettimiditymididevicesettings.h"

namespace Spinet
{

K_PLUGIN_FACTORY(KcmTimidityMidiDeviceSettingsFactory, registerPlugin<KcmTimidityMidiDeviceSettings>();)
K_EXPORT_PLUGIN(KcmTimidityMidiDeviceSettingsFactory(KAboutData("kcmspinettimiditymididevicesettings", "spinet",
                                          ki18n("Timidity Midi Device Settings"), "1.0")))

KcmTimidityMidiDeviceSettings::KcmTimidityMidiDeviceSettings(QWidget *parent, const QVariantList &args)
: KCModule(KcmTimidityMidiDeviceSettingsFactory::componentData(), parent, args)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    QWidget *widget = new QWidget;
    uiTimidityMidiDeviceSettings = new Ui::SpinetTimidityMidiDeviceSettings;
    uiTimidityMidiDeviceSettings->setupUi(widget);
    layout->addWidget(widget);

    addConfig(SpinetTimidityMidiDeviceSettings::self(), widget);

    load();
}

void KcmTimidityMidiDeviceSettings::load()
{
    KCModule::load();
}

void KcmTimidityMidiDeviceSettings::save()
{
    KCModule::save();
}

void KcmTimidityMidiDeviceSettings::defaults()
{
    KCModule::defaults();
}

}

