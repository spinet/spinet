/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef TIMIDITYMIDIDEVICE_H
#define TIMIDITYMIDIDEVICE_H

#include <QVariant>

#include <KDE/KProcess>

#include <interfaces/iplugin.h>
#include <interfaces/isoundcontroller/imididevice.h>

namespace Spinet
{

class TimidityMidiDevice : public IMidiDevice
{
    Q_OBJECT
public:
    explicit TimidityMidiDevice(QObject *parent, const QVariantList &args = QVariantList());
    virtual ~TimidityMidiDevice();
    virtual void unload();

    const QStringList &outputPorts();
    const QStringList &inputPorts();

private:
    bool check(); 

    KProcess m_timidityProcess;
    KProcess m_pmidiProcess;

    QStringList m_outputPorts;
    QStringList m_inputPorts;
};

}

#endif

