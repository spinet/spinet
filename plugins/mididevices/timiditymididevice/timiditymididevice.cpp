/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "timiditymididevice.h"

#include <KDE/KDebug>
#include <KDE/KStandardDirs>

#include "spinettimiditymididevicesettings.h"

namespace Spinet
{

K_PLUGIN_FACTORY(TimidityMidiDeviceFactory, registerPlugin<TimidityMidiDevice>();)
K_EXPORT_PLUGIN(TimidityMidiDeviceFactory(KAboutData("timiditymididevice", "timiditymididevice",
                                  ki18n("Spinet timidity MIDI device plugin"), "1.0",
                                  ki18n("Spinet timidity MIDI device plugin"),
                                  KAboutData::License_GPL)))

TimidityMidiDevice::TimidityMidiDevice(QObject *parent, const QVariantList &args)
: IMidiDevice(TimidityMidiDeviceFactory::componentData(), parent)
{
    Q_UNUSED(args);

    if (check())
    {
        m_inputPorts << "TiMidity:0" << "TiMidity:1" << "TiMidity:2" << "TiMidity:3";
        m_timidityProcess.setProgram(SpinetTimidityMidiDeviceSettings::self()->cmdTimidity().toLocalFile(), QStringList() << "-iA");
        m_timidityProcess.start();
    }
}

TimidityMidiDevice::~TimidityMidiDevice()
{
}

const QStringList &TimidityMidiDevice::outputPorts()
{
    return m_outputPorts;
}

const QStringList &TimidityMidiDevice::inputPorts()
{
    return m_inputPorts;
}

void TimidityMidiDevice::unload()
{
    if (m_timidityProcess.state() == KProcess::Running)
    {
        m_timidityProcess.kill();
        m_timidityProcess.waitForFinished(1000);
	kDebug() << "Timidity killed";
    }
    if (m_pmidiProcess.state() == KProcess::Running)
    {
        m_pmidiProcess.kill();
        m_pmidiProcess.waitForFinished(1000);
	kDebug() << "pmidi killed";
    }
}

bool TimidityMidiDevice::check()
{
    KUrl url = SpinetTimidityMidiDeviceSettings::self()->cmdTimidity();
    QString cmd = KGlobal::dirs()->findExe(url.toLocalFile());
    if (cmd.isEmpty())
    {
	kDebug() << "Timidity not found !";
        return false;
    }
    return true;
}

}
