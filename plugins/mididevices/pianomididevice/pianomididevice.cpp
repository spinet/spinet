 /********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "pianomididevice.h"

#include <QtGui/QGraphicsView>
#include <QtGui/QGraphicsRectItem>
#include <QtGui/QBrush>

#include <KDE/KDebug>
#include <KDE/KStandardDirs>

#include <drumstick/alsaclient.h>
#include <drumstick/alsaevent.h>
#include <drumstick/alsaport.h>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>

namespace Spinet
{

K_PLUGIN_FACTORY(PianoMidiDeviceFactory, registerPlugin<PianoMidiDevice>();)
K_EXPORT_PLUGIN(PianoMidiDeviceFactory(KAboutData("pianomididevice", "pianomididevice",
                                  ki18n("Spinet piano MIDI device plugin"), "1.0",
                                  ki18n("Spinet piano MIDI device plugin"),
                                  KAboutData::License_GPL)))

PianoMidiDevice::PianoMidiDevice(QObject *parent, const QVariantList &args)
: IMidiDevice(PianoMidiDeviceFactory::componentData(), parent)
{
    Q_UNUSED(args);
    
    m_inputPorts << "Piano:4";
  
    m_pianoDevice = new drumstick::MidiClient(this);
    m_pianoDevice->open();
    m_pianoDevice->setPoolOutput(50);
    m_pianoDevice->setClientName("Spinet Piano Device");
    connect(m_pianoDevice, SIGNAL(eventReceived(SequencerEvent*)), SLOT(sequencerEvent(SequencerEvent*)));
    
    m_inputPort = new drumstick::MidiPort(this);
    m_inputPort->attach(m_pianoDevice);
    m_inputPort->setPortName("Spinet Piano Device Output Port");
    m_inputPort->setCapability(SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE);
    m_inputPort->setPortType(SND_SEQ_PORT_TYPE_APPLICATION | SND_SEQ_PORT_TYPE_MIDI_GENERIC);
  
    m_pianoDevice->setRealTimeInput(false);
    m_pianoDevice->startSequencerInput();

    //*************************
    scene = new QGraphicsScene;
    view = new QGraphicsView(scene);
    view->setObjectName("pianoMidiDevice");

    int value = 21;
    QGraphicsRectItem *key;
    key = scene->addRect(0, 0, 21, 100);
    key->setPos(-26*21, 0);
    m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
    key = scene->addRect(0, 0, 14, 60, QPen(), QBrush(Qt::black));
    key->setPos(-26*21+14, 0);
    key->setZValue(1);
    m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::black));
    key = scene->addRect(0, 0, 21, 100);
    key->setPos(-26*21+21, 0);
    m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
    
    int i;
    for (i = 0; i < 7; ++i)
    {
      key = scene->addRect(0, 0, 21, 100);
      key->setPos(-26*21+42+(i*147), 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
      key = scene->addRect(0, 0, 14, 60, QPen(), QBrush(Qt::black));
      key->setPos(-26*21+63-7+(i*21*7), 0);
      key->setZValue(1);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::black));
      key = scene->addRect(0, 0, 21, 100);
      key->setPos(-26*21+42+(i*147)+21, 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
      key = scene->addRect(0, 0, 14, 60, QPen(), QBrush(Qt::black));
      key->setPos(-26*21+63-7+(i*21*7)+21, 0);
      key->setZValue(1);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::black));
      key = scene->addRect(0, 0, 21, 100);
      key->setPos(-26*21+42+(i*147)+42, 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
      key = scene->addRect(0, 0, 21, 100);
      key->setPos(-26*21+42+(i*147)+63, 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
      key = scene->addRect(0, 0, 14, 60, QPen(), QBrush(Qt::black));
      key->setZValue(1);
      key->setPos(-26*21+63-7+(i*21*7)+63, 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::black));
      key = scene->addRect(0, 0, 21, 100);
      key->setPos(-26*21+42+(i*147)+84, 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
      key = scene->addRect(0, 0, 14, 60, QPen(), QBrush(Qt::black));
      key->setPos(-26*21+63-7+(i*21*7)+84, 0);
      key->setZValue(1);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::black));
      key = scene->addRect(0, 0, 21, 100);
      key->setPos(-26*21+42+(i*147)+105, 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
      key = scene->addRect(0, 0, 14, 60, QPen(), QBrush(Qt::black));
      key->setPos(-26*21+63-7+(i*21*7)+105, 0);
      key->setZValue(1);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::black));
      key = scene->addRect(0, 0, 21, 100);
      key->setPos(-26*21+42+(i*147)+126, 0);
      m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
    }
    key = scene->addRect(0, 0, 21, 100);
    key->setPos(-26*21+42+((i-1)*147)+147, 0);
    m_keyboardMap.insert(value++, QPair<QGraphicsRectItem *, QColor>(key, Qt::white));
    
    ICore::self()->uiController()->addDockWidget("Virtual Piano", view, Qt::TopDockWidgetArea);
}

PianoMidiDevice::~PianoMidiDevice()
{
    delete scene;
    m_pianoDevice->stopSequencerInput();
    m_pianoDevice->close();
}

const QStringList &PianoMidiDevice::outputPorts()
{
    return m_outputPorts;
}

const QStringList &PianoMidiDevice::inputPorts()
{
    return m_inputPorts;
}


void PianoMidiDevice::unload()
{
}

bool PianoMidiDevice::check()
{
  return false;
}

/**
 * Altera a cor das teclas do piano.
 * 
 * @param event		Evento recolhido	
 * @author		Luis Paulo Torres
 */
void PianoMidiDevice::sequencerEvent(SequencerEvent *event)
{
  if (event->getSequencerType() == SND_SEQ_EVENT_NOTEON)
  {
    NoteOnEvent *noteOnEvent = dynamic_cast<NoteOnEvent *>(event);
    if (noteOnEvent)
    {
	if (noteOnEvent->getVelocity() != 0)
	    m_keyboardMap.value(noteOnEvent->getKey()).first->setBrush(QBrush(Qt::blue));
	else
	    m_keyboardMap.value(noteOnEvent->getKey()).first->setBrush(QBrush(m_keyboardMap.value(noteOnEvent->getKey()).second));
    }
  }
  if (event->getSequencerType() == SND_SEQ_EVENT_NOTEOFF)
  {
    NoteOffEvent *noteOffEvent = dynamic_cast<NoteOffEvent *>(event);
    if (noteOffEvent)
    {
	m_keyboardMap.value(noteOffEvent->getKey()).first->setBrush(QBrush(m_keyboardMap.value(noteOffEvent->getKey()).second));
    }
  }
}

}
