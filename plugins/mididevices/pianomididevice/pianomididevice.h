/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef PIANOMIDIDEVICE_H
#define PIANOMIDIDEVICE_H

#include <QtCore/QVariant>
#include <QtCore/QHash>
#include <QtCore/QPair>
//***********************
#include <QtGui/QGraphicsView>

#include <interfaces/iplugin.h>
#include <interfaces/isoundcontroller/imididevice.h>

class QGraphicsRectItem;

namespace drumstick
{
  class MidiClient;
  class MidiPort;
  class MidiQueue;
  class SequencerEvent;
}

using namespace drumstick;

namespace Spinet
{
  
class AlsaMidiPlayer;
  
class PianoMidiDevice : public IMidiDevice
{
  Q_OBJECT
public:
    explicit PianoMidiDevice(QObject *parent, const QVariantList &args = QVariantList());
    virtual ~PianoMidiDevice();
    virtual void unload();

    const QStringList &outputPorts();
    const QStringList &inputPorts();
  
public Q_SLOTS:
    void sequencerEvent(SequencerEvent *event);
  
private:
    bool check(); 
    int m_queueId;
    int m_inputPortId;
  
    MidiClient *m_pianoDevice;
    MidiPort   *m_inputPort;
    MidiQueue  *m_queue;

    QGraphicsScene *scene;
    QGraphicsView *view;
    
    QStringList m_outputPorts;
    QStringList m_inputPorts;
    QHash<int, QPair<QGraphicsRectItem *, QColor> > m_keyboardMap;
};

}

#endif
