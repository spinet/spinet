add_subdirectory(phononaudiosequencer)

if(CMAKE_SYSTEM MATCHES "Linux")
    add_subdirectory(alsamidisequencer)
endif(CMAKE_SYSTEM MATCHES "Linux")

if(CMAKE_SYSTEM MATCHES "Windows")
    add_subdirectory(winmcimidisequencer)
endif(CMAKE_SYSTEM MATCHES "Windows")
