/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "alsamidiplayer.h"

#include <QtCore/qmath.h>

#include <drumstick/alsaevent.h>
#include <drumstick/alsaclient.h>

#include "alsamidiobject.h"

#include <KDE/KDebug>

namespace Spinet
{
  
AlsaMidiPlayer::AlsaMidiPlayer(drumstick::MidiClient *midiDevice, int outputPortId, QObject *parent, AlsaMidiObject *alsaMidiObject)
: drumstick::SequencerOutputThread(midiDevice, outputPortId), m_midiDevice(midiDevice), m_outputPortId(outputPortId), m_initialPosition(0), m_pitchShift(0), m_echoResolution(0)
{
    setParent(parent);
    setAlsaMidiObject(alsaMidiObject);
    connect(this, SIGNAL(stopped()), SLOT(playerThreadStopped()));
}

AlsaMidiPlayer::~AlsaMidiPlayer()
{
    if (isRunning())
    {
        stop();
    }
}

void AlsaMidiPlayer::setAlsaMidiObject(AlsaMidiObject *alsaMidiObject)
{
    m_alsaMidiObject = alsaMidiObject;
    if (m_alsaMidiObject)
	m_echoResolution = m_alsaMidiObject-> division() / 12;
    else
	m_echoResolution = 0;
    setInitialPosition(0);
}

bool AlsaMidiPlayer::hasNext()
{
    return m_alsaMidiObject->hasNext();
}

drumstick::SequencerEvent *AlsaMidiPlayer::nextEvent()
{
    drumstick::SequencerEvent *event = m_alsaMidiObject->nextEvent()->clone();
    if (m_pitchShift != 0)
    {
	switch (event->getSequencerType())
	{
	    case SND_SEQ_EVENT_NOTE:
	    case SND_SEQ_EVENT_NOTEON:
	    case SND_SEQ_EVENT_NOTEOFF:
	    case SND_SEQ_EVENT_KEYPRESS:
	    {
		drumstick::KeyEvent *keyEvent = static_cast<drumstick::KeyEvent *>(event);
		if (keyEvent->getChannel() != MIDI_GM_DRUM_CHANNEL)
		    keyEvent->setKey(keyEvent->getKey() + m_pitchShift);
	    }
	    break;
	}
    }
    return event;
}

void AlsaMidiPlayer::setInitialPosition(unsigned int initialPosition)
{
    if (m_alsaMidiObject)
    {
	m_alsaMidiObject->resetPosition();
	while (m_alsaMidiObject->hasNext() && (m_alsaMidiObject->nextEvent()->getTick() < initialPosition));
	kDebug() << "Setando initialPosition para " << initialPosition;
	m_initialPosition = initialPosition;
    }
}

unsigned int AlsaMidiPlayer::getInitialPosition()
{
    return m_initialPosition;
}

void AlsaMidiPlayer::setVolumeFactor(float value)
{
    int volume = qFloor(100.0 * value);
    if (volume < 0) volume = 0;
    if (volume > 127) volume = 127;
    for(int channel = 0; channel < 16; ++channel)
    {
        drumstick::ControllerEvent ev(channel, MIDI_CTL_MSB_MAIN_VOLUME, volume);
	ev.setSource(m_outputPortId);
	ev.setSubscribers();
	ev.setDirect();
	sendSongEvent(&ev);
    }
}

void AlsaMidiPlayer::setPitchShift(int pitchShift)
{
    m_pitchShift = pitchShift;
}

void AlsaMidiPlayer::playerThreadStopped()
{
    for (int channel = 0; channel < 16; ++channel)
    {
	drumstick::ControllerEvent ev1(channel, MIDI_CTL_ALL_NOTES_OFF, 0);
	ev1.setSource(m_outputPortId);
	ev1.setSubscribers();
	ev1.setDirect();
	m_midiDevice->outputDirect(&ev1);
	drumstick::ControllerEvent ev2(channel, MIDI_CTL_ALL_SOUNDS_OFF, 0);
	ev2.setSource(m_outputPortId);
	ev2.setSubscribers();
	ev2.setDirect();
	m_midiDevice->outputDirect(&ev2);
    }
    m_midiDevice->drainOutput();
}

unsigned int AlsaMidiPlayer::getEchoResolution()
{
    return m_echoResolution;
}

}
