/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef ALSAMIDIOBJECT_H
#define ALSAMIDIOBJECT_H

#include <QtCore/QObject>

namespace drumstick
{
    class SequencerEvent;
}

namespace Spinet
{

class IMidiObject;

class AlsaMidiObject : public QObject
{
    Q_OBJECT

public:
    AlsaMidiObject(QObject *parent = 0);
    virtual ~AlsaMidiObject();

    virtual bool hasNext();
    virtual drumstick::SequencerEvent *nextEvent();

    virtual void resetPosition();
    virtual void clear();

    virtual int division() const;
    virtual int initialTempo() const;

    virtual void fromMidiObject(IMidiObject *midiObject, int outputPortId, int queueId);
    
    int lastTick() const;

private:
    QList<drumstick::SequencerEvent *> m_alsaMidiEvents;
    QList<drumstick::SequencerEvent *>::const_iterator m_iterator;

    int m_division;
    int m_initialTempo;
};

}

#endif
