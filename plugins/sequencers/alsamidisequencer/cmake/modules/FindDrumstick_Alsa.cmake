find_package(PkgConfig)
if(PKG_CONFIG_FOUND)
    pkg_check_modules(DRUMSTICK_ALSA ${REQUIRED} drumstick-alsa)
endif(PKG_CONFIG_FOUND)
