/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "alsamidisequencer.h"

#include <QtCore/qmath.h>

#include <drumstick/alsaclient.h>
#include <drumstick/alsaqueue.h>
#include <drumstick/alsaport.h>

#include <interfaces/icore.h>
#include <interfaces/isoundcontroller/isoundcontroller.h>
#include <interfaces/isoundcontroller/imidiobject.h>
#include <interfaces/isoundcontroller/imidievent.h>

#include "alsamidiplayer.h"

#include <KDE/KDebug>

namespace Spinet
{

K_PLUGIN_FACTORY(AlsaMidiSequencerFactory, registerPlugin<AlsaMidiSequencer>();)
K_EXPORT_PLUGIN(AlsaMidiSequencerFactory(KAboutData("alsamidisequencer", "alsamidisequencer",
                                  ki18n("Spinet ALSA MIDI sequencer plugin"), "1.0",
                                  ki18n("Spinet ALSA MIDI sequencer plugin"),
                                  KAboutData::License_GPL)))

AlsaMidiSequencer::AlsaMidiSequencer(QObject *parent, const QVariantList &args)
: IMidiSequencer(AlsaMidiSequencerFactory::componentData(), parent), m_alsaMidiObject(0), m_paused(false), m_tempoFactor(1), m_volumeFactor(1)
{
    Q_UNUSED(args);

    m_midiDevice = new drumstick::MidiClient(this);
    m_midiDevice->open();
    m_midiDevice->setPoolOutput(50);
    m_midiDevice->setClientName("Spinet ALSA MIDI Sequencer");
    connect(m_midiDevice, SIGNAL(eventReceived(SequencerEvent*)), SLOT(sequencerEvent(SequencerEvent*)));

    m_outputPort = new drumstick::MidiPort(this);
    m_outputPort->attach(m_midiDevice);
    m_outputPort->setPortName("Spinet ALSA MIDI Sequencer Output Port");
    m_outputPort->setCapability(SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ | SND_SEQ_PORT_CAP_WRITE);
    m_outputPort->setPortType(SND_SEQ_PORT_TYPE_APPLICATION | SND_SEQ_PORT_TYPE_MIDI_GENERIC);

    m_queue = m_midiDevice->createQueue("Spinet ALSA MIDI Sequencer");
    m_queueId = m_queue->getId();
    m_outputPortId = m_outputPort->getPortId();

    m_alsaMidiPlayer = new AlsaMidiPlayer(m_midiDevice, m_outputPortId, this);
    connect(m_alsaMidiPlayer, SIGNAL(finished()), SLOT(stop()));

    m_outputPorts << "Spinet ALSA MIDI Sequencer:Spinet ALSA MIDI Sequencer Output Port";
    m_midiDevice->setRealTimeInput(false);
    m_midiDevice->startSequencerInput();
}

void AlsaMidiSequencer::unload()
{
}

AlsaMidiSequencer::~AlsaMidiSequencer()
{
    m_midiDevice->stopSequencerInput();
    m_outputPort->detach();
    m_midiDevice->close();
    if (m_alsaMidiObject)
	delete m_alsaMidiObject;
}

const QStringList &AlsaMidiSequencer::outputPorts()
{
    return m_outputPorts;
}

const QStringList &AlsaMidiSequencer::inputPorts()
{
    return m_inputPorts;
}

void AlsaMidiSequencer::setMidiObjectInternal(IMidiObject *midiObject)
{
    stop();
    if (m_alsaMidiObject)
    {
	delete m_alsaMidiObject;
    }
    m_alsaMidiObject = new AlsaMidiObject;
    m_alsaMidiObject->fromMidiObject(midiObject, m_outputPortId, m_queueId);
    m_alsaMidiPlayer->setAlsaMidiObject(m_alsaMidiObject);
    midiObject->resetPosition();
    emit playingTrackingEvent(0, 6.0e7f / m_alsaMidiObject->initialTempo(), 0, 0, 0);
}

void AlsaMidiSequencer::playInternal()
{
    m_outputPort->subscribeTo("Spinet Piano Device:0");
    m_outputPort->subscribeTo("TiMidity:0");

    if (!m_alsaMidiPlayer->isRunning())
    {
        drumstick::QueueTempo firstTempo = m_queue->getTempo();
	setTempoFactorInternal(m_tempoFactor);
	kDebug() << "Volume: " << m_volumeFactor;
	setVolumeFactorInternal(m_volumeFactor);
	kDebug() << "INITIAL: " << m_alsaMidiPlayer->getInitialPosition();
	m_alsaMidiPlayer->start();
    }
}

void AlsaMidiSequencer::setPausedInternal(bool paused)
{
    kDebug();
    if (paused)
    {
	m_paused = true;
	m_alsaMidiPlayer->stop();
	m_alsaMidiPlayer->setInitialPosition(m_queue->getStatus().getTickTime());
    }
    else if (m_paused)
    {
	m_paused = false;
	m_alsaMidiPlayer->start();
    }
}

void AlsaMidiSequencer::stopInternal()
{
    kDebug();
    m_alsaMidiPlayer->stop();
    m_alsaMidiPlayer->setInitialPosition(0);
    m_paused = false;
    if (m_alsaMidiObject)
    {
	emit playingTrackingEvent(0, 6.0e7f / m_alsaMidiObject->initialTempo(), 0, 0, 0);
    }
}

void AlsaMidiSequencer::setTempoFactorInternal(float value)
{
    drumstick::QueueTempo qtempo = m_queue->getTempo();
    qtempo.setPPQ(m_alsaMidiObject->division());
    qtempo.setTempo(m_alsaMidiObject->initialTempo());
    qtempo.setTempoFactor(value);
    m_queue->setTempo(qtempo);
    m_midiDevice->drainOutput();
    m_tempoFactor = value;
}

void AlsaMidiSequencer::setVolumeFactorInternal(float value)
{
    m_alsaMidiPlayer->setVolumeFactor(value);
    m_volumeFactor = value;
}

void AlsaMidiSequencer::setPitchShiftInternal(int value)
{
    kDebug();
    m_alsaMidiPlayer->setPitchShift(value);
    if (m_alsaMidiPlayer->isRunning())
    {
        m_alsaMidiPlayer->stop();
        unsigned int position = m_queue->getStatus().getTickTime();
        m_queue->clear();
        m_alsaMidiPlayer->setInitialPosition(position);
        m_alsaMidiPlayer->start();
    }
}

void AlsaMidiSequencer::setPositionInternal(float value)
{
    kDebug();
    bool isRunning = m_alsaMidiPlayer->isRunning();
    m_alsaMidiPlayer->stop();
    m_alsaMidiPlayer->setInitialPosition(m_alsaMidiObject->lastTick()*value);
    if (isRunning)
	m_alsaMidiPlayer->start();
}

quint64 AlsaMidiSequencer::ticksToMiliSecs(quint64 ticks, quint16 division, quint64 tempo) const
{
    double result;
    double smpte_format;
    double smpte_resolution;

    if (division > 0)
    {
        result = static_cast<double>(ticks * tempo)/(division * 10000.0);
    }
    else
    {
        smpte_format = ((division >> 8) & 0xff);
        smpte_resolution = (division & 0xff);
        result = static_cast<double>(ticks)/(smpte_format * smpte_resolution * 10000.0);
    }
    return ((quint64)(result + 0.5));
}

void AlsaMidiSequencer::sequencerEvent(SequencerEvent *event)
{
    if ((event->getSequencerType() == SND_SEQ_EVENT_ECHO) && (m_alsaMidiObject->lastTick() != 0))
    {
        int pos = 100 * event->getTick() / m_alsaMidiObject->lastTick();
        quint64 msecs = ticksToMiliSecs(event->getTick(), m_alsaMidiObject->division(), m_alsaMidiObject->initialTempo());
        emit playingTrackingEvent(pos, m_queue->getTempo().getRealBPM(), msecs / 6000, (msecs/100) % 60, msecs % 100);
    }
    delete event;
}

}
