/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef ALSAMIDISEQUENCER_H
#define ALSAMIDISEQUENCER_H

#include <QVariant>
#include <QStringList>

#include <interfaces/iplugin.h>
#include <interfaces/isoundcontroller/imidisequencer.h>

#include "alsamidiobject.h"

namespace drumstick
{
    class MidiClient;
    class MidiPort;
    class MidiQueue;
}

using namespace drumstick;

namespace Spinet
{

class AlsaMidiPlayer;

class AlsaMidiSequencer : public IMidiSequencer
{
    Q_OBJECT
public:
    explicit AlsaMidiSequencer(QObject *parent, const QVariantList &args = QVariantList());
    virtual ~AlsaMidiSequencer();
    virtual void unload();

    virtual const QStringList &outputPorts();
    virtual const QStringList &inputPorts ();

public Q_SLOTS:
    void sequencerEvent(SequencerEvent *event);

protected:
    virtual void setMidiObjectInternal(IMidiObject *midiObject);
    virtual void playInternal ();
    virtual void setPausedInternal(bool paused);
    virtual void stopInternal ();
    virtual void setTempoFactorInternal(float value);
    virtual void setVolumeFactorInternal(float value);
    virtual void setPitchShiftInternal(int value);
    virtual void setPositionInternal(float value);

private:
    quint64 ticksToMiliSecs(quint64 ticks, quint16 division, quint64 tempo) const;

    QStringList m_outputPorts;
    QStringList m_inputPorts;

    AlsaMidiPlayer *m_alsaMidiPlayer;
    AlsaMidiObject *m_alsaMidiObject;
    
    drumstick::MidiClient *m_midiDevice;
    drumstick::MidiPort   *m_outputPort;
    drumstick::MidiQueue  *m_queue;
    
    int m_outputPortId;
    int m_queueId;
    bool m_paused;
    
    float m_tempoFactor;
    float m_volumeFactor;
};

}

#endif
