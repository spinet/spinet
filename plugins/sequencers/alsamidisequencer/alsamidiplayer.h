/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef ALSAMIDIPLAYER_H
#define ALSAMIDIPLAYER_H

#include <drumstick/playthread.h>
#include <interfaces/icore.h>

namespace drumstick
{
    class MidiClient;
    class SequencerEvent;
}

namespace Spinet
{

class AlsaMidiObject;

class AlsaMidiPlayer : public drumstick::SequencerOutputThread
{
    Q_OBJECT
public:
    explicit AlsaMidiPlayer(drumstick::MidiClient *midiDevice, int outputPortId, QObject *parent = 0, AlsaMidiObject *alsaMidiObject = 0);
    virtual ~AlsaMidiPlayer();

    virtual void setAlsaMidiObject(AlsaMidiObject *alsaMidiObject);

    virtual bool hasNext();
    virtual drumstick::SequencerEvent *nextEvent();

    void setInitialPosition(unsigned int initialPosition);
    virtual unsigned int getInitialPosition();
    
    void setVolumeFactor(float value);
    void setPitchShift(int pitchShift);
    
    virtual unsigned int getEchoResolution();

private Q_SLOTS:
    void playerThreadStopped();
  
private:
  
    drumstick::MidiClient *m_midiDevice;
    int m_outputPortId;
    unsigned int m_initialPosition;
    
    AlsaMidiObject *m_alsaMidiObject;
    int m_pitchShift;
    unsigned int m_echoResolution;
};

}

#endif
