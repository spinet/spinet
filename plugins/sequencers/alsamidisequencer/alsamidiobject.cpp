/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "alsamidiobject.h"

#include <drumstick/alsaevent.h>

#include <interfaces/isoundcontroller/imidiobject.h>
#include <interfaces/isoundcontroller/imidievent.h>

namespace Spinet
{

AlsaMidiObject::AlsaMidiObject(QObject *parent)
: QObject(parent), m_division(0), m_initialTempo(0)
{
    m_iterator = m_alsaMidiEvents.constBegin();
}

AlsaMidiObject::~AlsaMidiObject()
{
}

bool AlsaMidiObject::hasNext()
{
    return m_iterator != m_alsaMidiEvents.constEnd();
}

drumstick::SequencerEvent *AlsaMidiObject::nextEvent()
{
    drumstick::SequencerEvent *event = *m_iterator;
    m_iterator++;
    return event;
}

void AlsaMidiObject::resetPosition()
{
    m_iterator = m_alsaMidiEvents.constBegin();
}

void AlsaMidiObject::clear()
{
    m_alsaMidiEvents.clear();
    resetPosition();
}

int AlsaMidiObject::division() const
{
   return m_division;
}

int AlsaMidiObject::initialTempo() const
{
   return m_initialTempo;
}

void AlsaMidiObject::fromMidiObject(IMidiObject *midiObject, int outputPortId, int queueId)
{
    m_alsaMidiEvents.clear();
    midiObject->resetPosition();
    drumstick::SequencerEvent *event;
    while(midiObject->hasNext())
    {
	IMidiEvent *midiEvent = midiObject->nextEvent();
        event = 0;
	switch(midiEvent->type())
	{
	    case IMidiEvent::Undefined: break;
	    case IMidiEvent::SMFError: break;
	    case IMidiEvent::SMFHeader: m_division = midiEvent->param(2); break;
	    case IMidiEvent::SMFNoteOn: event = new drumstick::NoteOnEvent(midiEvent->param(0), midiEvent->param(1), midiEvent->param(2)); break;
	    case IMidiEvent::SMFNoteOff: event = new drumstick::NoteOffEvent(midiEvent->param(0), midiEvent->param(1), midiEvent->param(2)); break;
	    case IMidiEvent::SMFKeyPress: event = new drumstick::KeyPressEvent(midiEvent->param(0), midiEvent->param(1), midiEvent->param(2)); break;
	    case IMidiEvent::SMFCtlChange: event = new drumstick::ControllerEvent(midiEvent->param(0), midiEvent->param(1), midiEvent->param(2)); break;
	    case IMidiEvent::SMFPitchBend: event = new drumstick::PitchBendEvent(midiEvent->param(0), midiEvent->param(1)); break;
	    case IMidiEvent::SMFProgram: event = new drumstick::ProgramChangeEvent(midiEvent->param(0), midiEvent->param(1)); break;
	    case IMidiEvent::SMFChanPress: event = new drumstick::ChanPressEvent(midiEvent->param(0), midiEvent->param(1)); break;
	    case IMidiEvent::SMFSysex: event = new drumstick::SysExEvent(midiEvent->bytearrayData()); break;
	    case IMidiEvent::SMFSeqSpecific: break;
	    case IMidiEvent::SMFMetaUnregistered: break;
	    case IMidiEvent::SMFMetaMisc: break;
	    case IMidiEvent::SMFSequenceNum: break;
	    case IMidiEvent::SMFforcedChannel: break;
	    case IMidiEvent::SMFforcedPort: break;
	    case IMidiEvent::SMFText: break;
	    case IMidiEvent::SMFSmpte: break;
	    case IMidiEvent::SMFTimeSig: event = new drumstick::SequencerEvent(); break;
	    case IMidiEvent::SMFKeySig: break;
	    case IMidiEvent::SMFTempo: if (m_initialTempo == 0) { m_initialTempo = midiEvent->param(0); event = new drumstick::TempoEvent(queueId, m_initialTempo); } break;
	    case IMidiEvent::SMFendOfTrack: break;
	    case IMidiEvent::SMFTrackStart: break;
	    case IMidiEvent::SMFTrackEnd: break;
	    case IMidiEvent::SMFWriteTempoTrack: break;
	    case IMidiEvent::SMFWriteTrack: break;
	};
	if (event)
	{
	    event->setSource(outputPortId);
            if (event->getSequencerType() != SND_SEQ_EVENT_TEMPO)
            {
                event->setSubscribers();
            }
	    event->scheduleTick(queueId, midiEvent->tick(), false);
	    m_alsaMidiEvents.append(event);
	}
    }
    resetPosition();
}

int AlsaMidiObject::lastTick() const
{
    return m_alsaMidiEvents.last()->getTick();
}

}
