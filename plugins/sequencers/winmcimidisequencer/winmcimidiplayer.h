/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef WINMCIMIDIPLAYER_H
#define WINMCIMIDIPLAYER_H

#include <QtCore/QObject>

#include <windows.h>
#include <mmsystem.h>
#include <alsamidisequencer/alsamidiplayer.h>

namespace Spinet
{

class WinMciMidiObject;

class WinMciMidiPlayer : public QObject
{
    Q_OBJECT
public:
    explicit WinMciMidiPlayer(WinMciMidiObject *winMciMidiObject = 0);
    virtual ~WinMciMidiPlayer();

    virtual void setWinMciMidiObject(WinMciMidiObject *winMciMidiObject);
	void play();
	
private:
    WinMciMidiObject *m_winMciMidiObject;
	unsigned int m_device;
	HMIDISTRM m_handle;
	MIDIHDR m_midiStreamHdr;
	
	QString midiOutErrorMsg(unsigned long err);
};

}

#endif
