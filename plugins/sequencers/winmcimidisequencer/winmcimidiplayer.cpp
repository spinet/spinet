/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "winmcimidiplayer.h"

#include "winmcimidiobject.h"

#include <KDE/KMessageBox>
#include <stdio.h>
#include <conio.h>

namespace Spinet
{

WinMciMidiPlayer::WinMciMidiPlayer(WinMciMidiObject *winMciMidiObject)
{
    setWinMciMidiObject(winMciMidiObject);
}

WinMciMidiPlayer::~WinMciMidiPlayer()
{
}

void WinMciMidiPlayer::setWinMciMidiObject(WinMciMidiObject *winMciMidiObject)
{
    m_winMciMidiObject = winMciMidiObject;
}

void WinMciMidiPlayer::play()
{
    const QVarLengthArray<DWORD, STREAM_BUF_MAX> &streamBuffer = m_winMciMidiObject->streamBuffer();

	if (!streamBuffer.isEmpty())
	{
		m_device = 0;
		unsigned int err, tempo;
		err = midiStreamOpen(&m_handle, &m_device, 1, 0, 0, CALLBACK_NULL);
		if (err)
		{
			KMessageBox::error(0, midiOutErrorMsg(err));
			return;
		}

		MIDIPROPTIMEDIV prop;
		prop.cbStruct = sizeof(MIDIPROPTIMEDIV);
		prop.dwTimeDiv = m_winMciMidiObject->division();
		err = midiStreamProperty(m_handle, (LPBYTE)&prop, MIDIPROP_SET|MIDIPROP_TIMEDIV);
		if (err)
		{
			KMessageBox::error(0, "midiStreamProperty: " + midiOutErrorMsg(err));
			return;
		}

		m_midiStreamHdr.dwBufferLength  = streamBuffer.size()*sizeof(DWORD);
		m_midiStreamHdr.dwBytesRecorded = m_midiStreamHdr.dwBufferLength;
		m_midiStreamHdr.lpData = (LPSTR) streamBuffer.data();

		//m_midiStreamHdr.dwUser = 0;
		m_midiStreamHdr.dwFlags = 0;
		err = midiOutPrepareHeader((HMIDIOUT) m_handle, &m_midiStreamHdr, sizeof(MIDIHDR));
		if (err)
		{
			KMessageBox::error(0, "midiOutPrepareHeader: " + midiOutErrorMsg(err));
			return;
		}

		err = midiStreamOut(m_handle, &m_midiStreamHdr, sizeof(MIDIHDR));
		if (err)
		{
			KMessageBox::error(0, "midiStreamOut: " + midiOutErrorMsg(err));
			return;
		}

		err = midiStreamRestart(m_handle);
		if (err)
		{
			KMessageBox::error(0, "midiStreamRestart: " + midiOutErrorMsg(err));
			return;
		}
		while (MIDIERR_STILLPLAYING == midiOutUnprepareHeader((HMIDIOUT) m_handle, &m_midiStreamHdr, sizeof(MIDIHDR)));
		midiStreamClose(m_handle);
	}
}

QString WinMciMidiPlayer::midiOutErrorMsg(unsigned long err)
{
    #define BUFFERSIZE 121
	WCHAR buffer[BUFFERSIZE];
	
	if (!(err = midiOutGetErrorText(err, &buffer[0], BUFFERSIZE)))
		return QString::fromWCharArray(buffer);
	else
		return QString("Could not get error message !");
}

}
