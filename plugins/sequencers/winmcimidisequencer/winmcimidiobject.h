/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef WINMCIMIDIOBJECT_H
#define WINMCIMIDIOBJECT_H

#include <QtCore/QObject>
#include <QtCore/QVarLengthArray>

#include <windows.h>
#include <mmsystem.h>

#define STREAM_BUF_MAX 2000

#define MIDI_STATUS_NOTEOFF		0x80
#define MIDI_STATUS_NOTEON		0x90
#define MIDI_STATUS_POLYAFTER	0xA0
#define MIDI_STATUS_CTRLCHANGE	0xB0
#define MIDI_STATUS_PRGCHANGE	0xC0
#define MIDI_STATUS_CHANAFTER	0xD0
#define MIDI_STATUS_PITCHRANGE	0xE0
#define MIDI_STATUS_SYSEX		0xF0

#define MIDI_CHANNEL_MASK		0xE0

#define MIDI_META_TEXT			1
#define MIDI_META_COPYRIGHT		2
#define MIDI_META_TRKNAME		3
#define MIDI_META_INSTRNAME		4
#define MIDI_META_LYRIC			5
#define MIDI_META_MARKER		6
#define MIDI_META_CUE			7

namespace Spinet
{

class IMidiObject;

class WinMciMidiObject : public QObject
{
    Q_OBJECT

public:
    WinMciMidiObject(QObject *parent = 0);
    virtual ~WinMciMidiObject();

	virtual int division() const;
	virtual int initialTempo() const;
	virtual const QVarLengthArray<DWORD, STREAM_BUF_MAX> &streamBuffer() const;
		
    virtual void fromMidiObject(IMidiObject *midiObject);

    typedef union
	{
		DWORD  dwEvent;
		quint8 uiEvent[sizeof(DWORD)];
	} MIDIPACK;

private:
	QVarLengthArray<DWORD, STREAM_BUF_MAX> m_streamBuffer;
	int m_division;
	int m_initialTempo;

    QString midiOutErrorMsg(unsigned long err);
};

}

#endif
