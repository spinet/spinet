/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "winmcimidiobject.h"

#include <interfaces/isoundcontroller/imidiobject.h>
#include <interfaces/isoundcontroller/imidievent.h>

#include <KDE/KDebug>
#include <KDE/KMessageBox>

namespace Spinet
{

WinMciMidiObject::WinMciMidiObject(QObject *parent)
: QObject(parent)
{
}

WinMciMidiObject::~WinMciMidiObject()
{
}

int WinMciMidiObject::division() const
{
    return m_division;
}

int WinMciMidiObject::initialTempo() const
{
    return m_initialTempo;
}

const QVarLengthArray<DWORD, STREAM_BUF_MAX> &WinMciMidiObject::streamBuffer() const
{
    return m_streamBuffer;
}

void WinMciMidiObject::fromMidiObject(IMidiObject *midiObject)
{
    m_streamBuffer.clear();
    midiObject->resetPosition();
	MIDIPACK midiPack;
	long lastTick = 0;
    while(midiObject->hasNext())
    {
	IMidiEvent *midiEvent = midiObject->nextEvent();
        midiPack.dwEvent = (DWORD) 0;
		switch(midiEvent->type())
		{
			case IMidiEvent::Undefined:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> Undefined");
				break;
			case IMidiEvent::SMFError:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFError: stringData: " + midiEvent->stringData());
				break;
			case IMidiEvent::SMFHeader:
				m_division = midiEvent->param(2);
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFHeader. Format: " + QString::number(midiEvent->param(0)) + ", NroTracks: " + QString::number(midiEvent->param(1)) + ", Division: " + QString::number(m_division));
				break;
			case IMidiEvent::SMFNoteOn:
				midiPack.uiEvent[3] = MEVT_SHORTMSG;
				midiPack.uiEvent[2] = midiEvent->param(2);												// Velocity
				midiPack.uiEvent[1] = midiEvent->param(1);												// Pitch
				midiPack.uiEvent[0] = MIDI_STATUS_NOTEON | (midiEvent->param(0) & MIDI_CHANNEL_MASK);	// MIDI Event
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> NoteOn. Channel: " + QString::number(midiEvent->param(0)) + ", Pitch: " + QString::number(midiEvent->param(1)) + ", Vol: " + QString::number(midiEvent->param(2)));
				break;
			case IMidiEvent::SMFNoteOff:
				midiPack.uiEvent[3] = MEVT_SHORTMSG;
				midiPack.uiEvent[2] = midiEvent->param(2);												// Velocity
				midiPack.uiEvent[1] = midiEvent->param(1);												// Pitch
				midiPack.uiEvent[0] = MIDI_STATUS_NOTEOFF | (midiEvent->param(0) & MIDI_CHANNEL_MASK);	// MIDI Event
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> NoteOff. Channel: " + QString::number(midiEvent->param(0)) + ", Pitch: " + QString::number(midiEvent->param(1)) + ", Vol: " + QString::number(midiEvent->param(2)));
				break;
			case IMidiEvent::SMFKeyPress:
				midiPack.uiEvent[3] = MEVT_SHORTMSG;
				midiPack.uiEvent[2] = midiEvent->param(2);												// Pressure
				midiPack.uiEvent[1] = midiEvent->param(1);												// Pitch
				midiPack.uiEvent[0] = MIDI_STATUS_POLYAFTER | (midiEvent->param(0) & MIDI_CHANNEL_MASK);// MIDI Event
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> KeyPress. Channel: " + QString::number(midiEvent->param(0)) + ", Pitch: " + QString::number(midiEvent->param(1)) + ", Pressure: " + QString::number(midiEvent->param(2)));
				break;
			case IMidiEvent::SMFCtlChange:
				midiPack.uiEvent[3] = MEVT_SHORTMSG;
				midiPack.uiEvent[2] = midiEvent->param(2);													// Value
				midiPack.uiEvent[1] = midiEvent->param(1);													// Control
				midiPack.uiEvent[0] = MIDI_STATUS_CTRLCHANGE | (midiEvent->param(0) & MIDI_CHANNEL_MASK);	// MIDI Event
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> CtlChange. Channel: " + QString::number(midiEvent->param(0)) + ", Control: " + QString::number(midiEvent->param(1)) + ", Value: " + QString::number(midiEvent->param(2)));
				break;
			case IMidiEvent::SMFPitchBend:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> PitchBens. Channel: " + QString::number(midiEvent->param(0)) + ", Value: " + QString::number(midiEvent->param(1)));
				break;
			case IMidiEvent::SMFProgram:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> Program. Channel: " + QString::number(midiEvent->param(0)) + ", Patch: " + QString::number(midiEvent->param(1)));
				break;
			case IMidiEvent::SMFChanPress:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> ChanPress. Channel: " + QString::number(midiEvent->param(0)) + ", Pressure: " + QString::number(midiEvent->param(1)));
				break;
			case IMidiEvent::SMFSysex:
				MIDIPACK sysexMidiPack;
				sysexMidiPack.dwEvent = (MEVT_LONGMSG << 24) | sizeof(midiEvent->bytearrayData());
				m_streamBuffer.append((DWORD)(midiEvent->tick() - lastTick));
				m_streamBuffer.append(0);
				m_streamBuffer.append(midiPack.dwEvent);
				memcpy(&sysexMidiPack.dwEvent, midiEvent->bytearrayData().data(), sizeof(midiEvent->bytearrayData()));
				m_streamBuffer.append(midiPack.dwEvent);
				lastTick = midiEvent->tick();
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SysEx. ArrayData: " + midiEvent->bytearrayData());
				break;
			case IMidiEvent::SMFSeqSpecific:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SeqSpecific. ArrayData: " + midiEvent->bytearrayData());
				break;
			case IMidiEvent::SMFMetaUnregistered:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> MetaUnreg. Typ: " + QString::number(midiEvent->param(0)) + ", ArrayData: " + midiEvent->bytearrayData());
				break;
			case IMidiEvent::SMFMetaMisc:
				if (midiEvent->param(0) >= MIDI_META_TEXT && midiEvent->param(0) <= MIDI_META_CUE)
					//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> MetaMisc. Typ: " + QString::number(midiEvent->param(0)) + ", ArrayData: " + midiEvent->bytearrayData());
				break;
			case IMidiEvent::SMFSequenceNum:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SequenceNum. Seq: " + QString::number(midiEvent->param(0)));
				break;
			case IMidiEvent::SMFforcedChannel:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> forcedChannel. Channel: " + QString::number(midiEvent->param(0)));
				break;
			case IMidiEvent::SMFforcedPort:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> forcedPort. Port: " + QString::number(midiEvent->param(0)));
				break;
			case IMidiEvent::SMFText:
				// MIDI text events are a special case of MISC event and will be handled in MetaMisc slot
				break;
			case IMidiEvent::SMFSmpte:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> Smpte. " + QString::number(midiEvent->param(0)) + ", " + QString::number(midiEvent->param(1)) + ", " + QString::number(midiEvent->param(2)) + ", " + QString::number(midiEvent->param(3)) + ", " + QString::number(midiEvent->param(4)));
				break;
			case IMidiEvent::SMFTimeSig:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> TimeSig. " + QString::number(midiEvent->param(0)) + ", " + QString::number(midiEvent->param(1)) + ", " + QString::number(midiEvent->param(2)) + ", " + QString::number(midiEvent->param(3)));
				break;
			case IMidiEvent::SMFKeySig:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> KeySig. " + QString::number(midiEvent->param(0)) + ", " + QString::number(midiEvent->param(1)));
				break;
			case IMidiEvent::SMFTempo:
				m_initialTempo = midiEvent->param(0);
				midiPack.dwEvent = (DWORD) m_initialTempo;
				midiPack.uiEvent[3] = MEVT_TEMPO;
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFTempo. tempo: " + QString::number(midiEvent->param(0)));
				break;
			case IMidiEvent::SMFendOfTrack:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFendOfTrack");
				break;
			case IMidiEvent::SMFTrackStart:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFTrackStart");
				break;
			case IMidiEvent::SMFTrackEnd:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFTrackEnd");
				break;
			case IMidiEvent::SMFWriteTempoTrack:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFWriteTempoTrack");
				break;
			case IMidiEvent::SMFWriteTrack:
				//KMessageBox::error(0, QString::number((midiEvent->tick() - lastTick)) + " -> SMFWriteTrack. Track: " + QString::number(midiEvent->param(0)));
				break;
		};
		if (midiPack.dwEvent != 0)
		{
			m_streamBuffer.append((DWORD)(midiEvent->tick() - lastTick));
			m_streamBuffer.append(0);
			m_streamBuffer.append(midiPack.dwEvent);
			//KMessageBox::error(0, "Appending. Delay: " + QString::number(midiEvent->tick() - lastTick) + ", dwEvent: " + QString::number(midiPack.uiEvent[3]) + ", " + QString::number(midiPack.uiEvent[2]) + ", " + QString::number(midiPack.uiEvent[1]) + ", " + QString::number(midiPack.uiEvent[0]));
			lastTick = midiEvent->tick();
		}
    }
}

}
