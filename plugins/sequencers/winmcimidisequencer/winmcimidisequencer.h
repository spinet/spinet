/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef WINMCIMIDISEQUENCER_H
#define WINMCIMIDISEQUENCER_H

#include <QVariant>
#include <QStringList>

#include <interfaces/iplugin.h>
#include <interfaces/isoundcontroller/imidisequencer.h>

namespace Spinet
{

class WinMciMidiObject;
class WinMciMidiPlayer;

class WinMciMidiSequencer : public IMidiSequencer
{
    Q_OBJECT
public:
    explicit WinMciMidiSequencer(QObject *parent, const QVariantList &args = QVariantList());
    virtual ~WinMciMidiSequencer();
    virtual void unload();

    virtual const QStringList &outputPorts();
    virtual const QStringList &inputPorts ();

protected:
    virtual void setMidiObjectInternal(IMidiObject *midiObject);
    virtual void playInternal ();
    virtual void pauseInternal();
    virtual void stopInternal ();

private:
    QStringList m_outputPorts;
    QStringList m_inputPorts;

    WinMciMidiPlayer *m_winMciMidiPlayer;
    WinMciMidiObject *m_winMciMidiObject;
};

}

#endif
