/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "winmcimidisequencer.h"

#include <interfaces/icore.h>
#include <interfaces/isoundcontroller/isoundcontroller.h>
#include <interfaces/isoundcontroller/imidiobject.h>
#include <interfaces/isoundcontroller/imidievent.h>

#include "winmcimidiobject.h"
#include "winmcimidiplayer.h"

K_PLUGIN_FACTORY(WinMciMidiSequencerFactory, registerPlugin<WinMciMidiSequencer>();)
K_EXPORT_PLUGIN(WinMciMidiSequencerFactory(KAboutData("winmcimidisequencer", "winmcimidisequencer",
                                  ki18n("Spinet Windows MCI MIDI sequencer plugin"), "1.0",
                                  ki18n("Spinet Windows MCI MIDI sequencer plugin"),
                                  KAboutData::License_GPL)))

namespace Spinet
{

WinMciMidiSequencer::WinMciMidiSequencer(QObject *parent, const QVariantList &args)
: IMidiSequencer(WinMciMidiSequencerFactory::componentData(), parent)
{
    Q_UNUSED(args);
    
    m_winMciMidiPlayer = new WinMciMidiPlayer;
    m_outputPorts << "Spinet Windows MCI MIDI Sequencer:Spinet Windows MCI MIDI Sequencer Output Port";
}

void WinMciMidiSequencer::unload()
{
}

WinMciMidiSequencer::~WinMciMidiSequencer()
{
    delete m_winMciMidiPlayer;
}

const QStringList &WinMciMidiSequencer::outputPorts()
{
    return m_outputPorts;
}

const QStringList &WinMciMidiSequencer::inputPorts()
{
    return m_inputPorts;
}

void WinMciMidiSequencer::setMidiObjectInternal(IMidiObject *midiObject)
{
    stop();
    if (m_winMciMidiObject)
	delete m_winMciMidiObject;
    m_winMciMidiObject = new WinMciMidiObject;
    m_winMciMidiObject->fromMidiObject(midiObject);
    m_winMciMidiPlayer->setWinMciMidiObject(m_winMciMidiObject);
    emit midiObjectChanged(midiObject);
}

void WinMciMidiSequencer::playInternal()
{
    m_winMciMidiPlayer->play();
}

void WinMciMidiSequencer::pauseInternal()
{
}

void WinMciMidiSequencer::stopInternal()
{
}

}
