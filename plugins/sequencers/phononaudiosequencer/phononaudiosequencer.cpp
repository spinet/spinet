/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "phononaudiosequencer.h"

#include <Phonon/Global>
#include <Phonon/MediaObject>

namespace Spinet
{

K_PLUGIN_FACTORY(PhononAudioSequencerFactory, registerPlugin<PhononAudioSequencer>();)
K_EXPORT_PLUGIN(PhononAudioSequencerFactory(KAboutData("phononaudiosequencer", "phononaudiosequencer",
                                  ki18n("Spinet phonon audio sequencer plugin"), "1.0",
                                  ki18n("Spinet phonon audio sequencer plugin"),
                                  KAboutData::License_GPL)))

PhononAudioSequencer::PhononAudioSequencer(QObject *parent, const QVariantList &args)
: IAudioSequencer(PhononAudioSequencerFactory::componentData(), parent)
{
    Q_UNUSED(args);

    Phonon::MediaObject *music = Phonon::createPlayer(Phonon::MusicCategory, Phonon::MediaSource("./KDE-Im-Phone-Ring.wav"));
    music->play();
}

void PhononAudioSequencer::unload()
{
}

PhononAudioSequencer::~PhononAudioSequencer()
{
}

void PhononAudioSequencer::play()
{
}

void PhononAudioSequencer::setPaused(bool paused)
{
    Q_UNUSED(paused);
}

void PhononAudioSequencer::stop()
{
}

}
