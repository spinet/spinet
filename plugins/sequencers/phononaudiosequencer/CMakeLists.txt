set(phononaudiosequencer_SRCS
    phononaudiosequencer.cpp
)

kde4_add_plugin(phononaudiosequencer ${phononaudiosequencer_SRCS})
target_link_libraries(phononaudiosequencer ${KDE4_KDEUI_LIBS} ${KDE4_PHONON_LIBS} spinetinterfaces)

install(TARGETS phononaudiosequencer DESTINATION ${PLUGIN_INSTALL_DIR})

install(FILES spinetphononaudiosequencer.desktop DESTINATION ${SERVICES_INSTALL_DIR})
