/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef SMFFACTORY_H
#define SMFFACTORY_H

#include <QtCore/QObject>

#include <QVariant>

#include <interfaces/iplugin.h>
#include <interfaces/ismffactory.h>

namespace drumstick
{
    class QSmf;
}

namespace Spinet
{
  
class IMidiObject;

class SmfFactory : public IPlugin, public ISmfFactory
{
public:
    explicit SmfFactory(QObject *parent, const QVariantList &args = QVariantList());
    virtual ~SmfFactory();
    virtual void unload();

    IMidiObject *fromFile(const QString &fileName);

private Q_SLOTS:
    void slotOpenMidiFile();
    
    void slotSMFError(const QString &errorStr);
    void slotSMFHeader(int format, int ntrks, int division);
    void slotSMFNoteOn(int chan, int pitch, int vol);
    void slotSMFNoteOff(int chan, int pitch, int vol);
    void slotSMFKeyPress(int chan, int pitch, int press);
    void slotSMFCtlChange(int chan, int ctl, int value);
    void slotSMFPitchBend(int chan, int value);
    void slotSMFProgram(int chan, int patch);
    void slotSMFChanPress(int chan, int press);
    void slotSMFSysex(const QByteArray &data);
    void slotSMFSeqSpecific(const QByteArray &data);
    void slotSMFMetaUnregistered(int typ, const QByteArray &data);
    void slotSMFMetaMisc(int typ, const QByteArray &data);
    void slotSMFSequenceNum(int seq);
    void slotSMFforcedChannel(int channel);
    void slotSMFforcedPort(int port);
    void slotSMFText(int typ, const QString &data);
    void slotSMFSmpte(int b0, int b1, int b2, int b3, int b4);
    void slotSMFTimeSig(int b0, int b1, int b2, int b3);
    void slotSMFKeySig(int b0, int b1);
    void slotSMFTempo(int tempo);
    void slotSMFendOfTrack();
    void slotSMFTrackStart();
    void slotSMFTrackEnd();
    void slotSMFWriteTempoTrack();
    void slotSMFWriteTrack(int track);

private:
    drumstick::QSmf *m_smfReader;
    IMidiObject *m_midiObject;
};

}

#endif
