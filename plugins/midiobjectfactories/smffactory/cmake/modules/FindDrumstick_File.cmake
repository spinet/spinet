################################################################################
#   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          #
#                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> #
#                                                                              #
#   This program is free software; you can redistribute it and/or modify       #
#   it under the terms of the GNU General Public License as published by       #
#   the Free Software Foundation; either version 2 of the License, or          #
#   (at your option) any later version.                                        #
#                                                                              #
#   This program is distributed in the hope that it will be useful,            #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#   GNU General Public License for more details.                               #
#                                                                              #
#   You should have received a copy of the GNU General Public License          #
#   along with this program; if not, write to the                              #
#   Free Software Foundation, Inc.,                                            #
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             #
################################################################################

if(CMAKE_SYSTEM MATCHES "Linux")
    find_package(PkgConfig)
    if(PKG_CONFIG_FOUND)
        pkg_check_modules(DRUMSTICK_FILE ${REQUIRED} drumstick-file)
    endif(PKG_CONFIG_FOUND)
else(CMAKE_SYSTEM MATCHES "Linux")
    set(DRUMSTICK_HEADERS drumstick/macros.h drumstick/qove.h drumstick/qsmf.h drumstick/qwrk.h)
    FIND_PATH(DRUMSTICK_FILE_INCLUDE_DIRS ${DRUMSTICK_HEADERS})
    if(DRUMSTICK_FILE_INCLUDE_DIRS)
        MESSAGE("-- Found drumstick-file include dir: ${DRUMSTICK_FILE_INCLUDE_DIRS}")
    else(DRUMSTICK_FILE_INCLUDE_DIRS)
        MESSAGE("-- drumstick-file include dir NOT FOUND !!")
    endif(DRUMSTICK_FILE_INCLUDE_DIRS)
    FIND_LIBRARY(DRUMSTICK_FILE_LIBRARIES drumstick-file)
    if(DRUMSTICK_FILE_LIBRARIES)
	    MESSAGE("-- Found drumstick-file library: ${DRUMSTICK_FILE_LIBRARIES}")
    else(DRUMSTICK_FILE_LIBRARIES)
        MESSAGE("-- drumstick-file library NOT FOUND !!")
    endif(DRUMSTICK_FILE_LIBRARIES)
endif(CMAKE_SYSTEM MATCHES "Linux")
