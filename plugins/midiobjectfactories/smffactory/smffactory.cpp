/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "smffactory.h"

#include <QDebug>

#include <KDE/KIcon>
#include <KDE/KAction>
#include <KDE/KActionCollection>
#include <KDE/KFileDialog>

#include <drumstick/qsmf.h>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/isoundcontroller/isoundcontroller.h>
#include <interfaces/isoundcontroller/imidisequencer.h>
#include <interfaces/isoundcontroller/imidiobject.h>

namespace Spinet
{

K_PLUGIN_FACTORY(SmfFactoryFactory, registerPlugin<SmfFactory>();)
K_EXPORT_PLUGIN(SmfFactoryFactory(KAboutData("smffactory", "smffactory",
                                  ki18n("Spinet SMF MIDI object factory plugin"), "1.0",
                                  ki18n("Spinet SMF MIDI object factory plugin"),
                                  KAboutData::License_GPL)))

SmfFactory::SmfFactory(QObject *parent, const QVariantList &args)
: IPlugin(SmfFactoryFactory::componentData(), parent), ISmfFactory(), m_smfReader(new drumstick::QSmf), m_midiObject(0)
{
    Q_UNUSED(args);

    setXMLFile("smffactoryui.rc");
    
    KAction *midiFileOpen = new KAction(KIcon("document-open"), i18n("Open &Midi"), this);
    actionCollection()->addAction(QLatin1String("open_midifile"), midiFileOpen);
    connect(midiFileOpen, SIGNAL(triggered(bool)), SLOT(slotOpenMidiFile()));
    
    connect(m_smfReader, SIGNAL(signalSMFError(QString)), SLOT(slotSMFError(QString)));
    connect(m_smfReader, SIGNAL(signalSMFHeader(int,int,int)), SLOT(slotSMFHeader(int,int,int)));
    connect(m_smfReader, SIGNAL(signalSMFNoteOn(int,int,int)), SLOT(slotSMFNoteOn(int,int,int)));
    connect(m_smfReader, SIGNAL(signalSMFNoteOff(int,int,int)), SLOT(slotSMFNoteOff(int,int,int)));
    connect(m_smfReader, SIGNAL(signalSMFKeyPress(int,int,int)), SLOT(slotSMFKeyPress(int,int,int)));
    connect(m_smfReader, SIGNAL(signalSMFCtlChange(int,int,int)), SLOT(slotSMFCtlChange(int,int,int)));
    connect(m_smfReader, SIGNAL(signalSMFPitchBend(int,int)), SLOT(slotSMFPitchBend(int,int)));
    connect(m_smfReader, SIGNAL(signalSMFProgram(int,int)), SLOT(slotSMFProgram(int,int)));
    connect(m_smfReader, SIGNAL(signalSMFChanPress(int,int)), SLOT(slotSMFChanPress(int,int)));
    connect(m_smfReader, SIGNAL(signalSMFSysex(QByteArray)), SLOT(slotSMFSysex(QByteArray)));
    connect(m_smfReader, SIGNAL(signalSMFSeqSpecific(QByteArray)), SLOT(slotSMFSeqSpecific(QByteArray)));
    connect(m_smfReader, SIGNAL(signalSMFMetaUnregistered(int,QByteArray)), SLOT(slotSMFMetaUnregistered(int,QByteArray)));
    connect(m_smfReader, SIGNAL(signalSMFMetaMisc(int,QByteArray)), SLOT(slotSMFMetaMisc(int,QByteArray)));
    connect(m_smfReader, SIGNAL(signalSMFSequenceNum(int)), SLOT(slotSMFSequenceNum(int)));
    connect(m_smfReader, SIGNAL(signalSMFforcedChannel(int)), SLOT(slotSMFforcedChannel(int)));
    connect(m_smfReader, SIGNAL(signalSMFforcedPort(int)), SLOT(slotSMFforcedPort(int)));
    connect(m_smfReader, SIGNAL(signalSMFText(int,QString)), SLOT(slotSMFText(int,QString)));
    connect(m_smfReader, SIGNAL(signalSMFSmpte(int,int,int,int,int)), SLOT(slotSMFSmpte(int,int,int,int,int)));
    connect(m_smfReader, SIGNAL(signalSMFTimeSig(int,int,int,int)), SLOT(slotSMFTimeSig(int,int,int,int)));
    connect(m_smfReader, SIGNAL(signalSMFKeySig(int,int)), SLOT(slotSMFKeySig(int,int)));
    connect(m_smfReader, SIGNAL(signalSMFTempo(int)), SLOT(slotSMFTempo(int)));
    connect(m_smfReader, SIGNAL(signalSMFendOfTrack()), SLOT(slotSMFendOfTrack()));
    connect(m_smfReader, SIGNAL(signalSMFTrackStart()), SLOT(slotSMFTrackStart()));
    connect(m_smfReader, SIGNAL(signalSMFTrackEnd()), SLOT(slotSMFTrackEnd()));
    connect(m_smfReader, SIGNAL(signalSMFWriteTempoTrack()), SLOT(slotSMFWriteTempoTrack()));
    connect(m_smfReader, SIGNAL(signalSMFWriteTrack(int)), SLOT(slotSMFWriteTrack(int)));
}

SmfFactory::~SmfFactory()
{
    delete m_smfReader;
}

void SmfFactory::unload()
{
}

IMidiObject *SmfFactory::fromFile(const QString &fileName)
{
    m_midiObject = ICore::self()->soundController()->newMidiObject();
    m_smfReader->readFromFile(fileName);
    m_midiObject->sort();
    m_midiObject->resetPosition();
    return m_midiObject;
}

void SmfFactory::slotOpenMidiFile()
{
    QString fileName = KFileDialog::getOpenFileName(KUrl(), "*.mid");
    if(!fileName.isEmpty())
    {
	fromFile(fileName);
	ICore::self()->soundController()->midiSequencers()[0]->setMidiObject(m_midiObject);
    }
}

void SmfFactory::slotSMFError(const QString &errorStr)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFError, errorStr));
}

void SmfFactory::slotSMFHeader(int format, int ntrks, int division)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFHeader, format, ntrks, division));
}

void SmfFactory::slotSMFNoteOn(int chan, int pitch, int vol)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFNoteOn, chan, pitch, vol));
}

void SmfFactory::slotSMFNoteOff(int chan, int pitch, int vol)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFNoteOff, chan, pitch, vol));
}

void SmfFactory::slotSMFKeyPress(int chan, int pitch, int press)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFKeyPress, chan, pitch, press));
}

void SmfFactory::slotSMFCtlChange(int chan, int ctl, int value)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFCtlChange, chan, ctl, value));
}

void SmfFactory::slotSMFPitchBend(int chan, int value)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFPitchBend, chan, value));
}

void SmfFactory::slotSMFProgram(int chan, int patch)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFProgram, chan, patch));
}

void SmfFactory::slotSMFChanPress(int chan, int press)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFChanPress, chan, press));
}

void SmfFactory::slotSMFSysex(const QByteArray &data)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFSysex, data));
}

void SmfFactory::slotSMFSeqSpecific(const QByteArray &data)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFSeqSpecific, data));
}

void SmfFactory::slotSMFMetaUnregistered(int typ, const QByteArray &data)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFMetaUnregistered, typ, data));
}

void SmfFactory::slotSMFMetaMisc(int typ, const QByteArray &data)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFMetaMisc, typ, data));
}

void SmfFactory::slotSMFSequenceNum(int seq)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFSequenceNum, seq));
}

void SmfFactory::slotSMFforcedChannel(int channel)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFforcedChannel, channel));
}

void SmfFactory::slotSMFforcedPort(int port)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFforcedPort, port));
}

void SmfFactory::slotSMFText(int typ, const QString &data)
{
    // MIDI text events are a special case of MISC event and will be handled in MetaMisc slot
    Q_UNUSED(typ);
    Q_UNUSED(data);
}

void SmfFactory::slotSMFSmpte(int b0, int b1, int b2, int b3, int b4)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFSmpte, b0, b1, b2, b3, b4));
}

void SmfFactory::slotSMFTimeSig(int b0, int b1, int b2, int b3)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFTimeSig, b0, b1, b2, b3));
}

void SmfFactory::slotSMFKeySig(int b0, int b1)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFKeySig, b0, b1));
}

void SmfFactory::slotSMFTempo(int tempo)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFTempo, tempo));
}

void SmfFactory::slotSMFendOfTrack()
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFendOfTrack));
}

void SmfFactory::slotSMFTrackStart()
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFTrackStart));
}

void SmfFactory::slotSMFTrackEnd()
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFTrackEnd));
}

void SmfFactory::slotSMFWriteTempoTrack()
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFWriteTempoTrack));
}

void SmfFactory::slotSMFWriteTrack(int track)
{
    m_midiObject->appendEvent(ICore::self()->soundController()->newMidiEvent(m_smfReader->getCurrentTime(), IMidiEvent::SMFWriteTrack, track));
}

}
