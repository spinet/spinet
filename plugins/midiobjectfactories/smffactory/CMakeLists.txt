set(smffactory_SRCS
    smffactory.cpp
)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
    ${SPINET_SOURCE_DIR}/plugins/midiobjectfactories/smffactory/cmake/modules
)

find_package(Drumstick_File REQUIRED)

include_directories(${DRUMSTICK_FILE_INCLUDE_DIRS})

kde4_add_plugin(smffactory ${smffactory_SRCS})
target_link_libraries(smffactory ${KDE4_KDEUI_LIBS} ${KDE4_KIO_LIBS} ${DRUMSTICK_FILE_LIBRARIES} spinetinterfaces)

install(TARGETS smffactory DESTINATION ${PLUGIN_INSTALL_DIR})

install(FILES spinetsmffactory.desktop DESTINATION ${SERVICES_INSTALL_DIR})

install(FILES smffactoryui.rc DESTINATION ${DATA_INSTALL_DIR}/smffactory)
