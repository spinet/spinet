/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "musicsheetgui.h"
#include "keynotes.h"

#include <QtDeclarative/QDeclarativeView>
#include <QList>
#include <KDebug>
#include <KStandardDirs>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/isoundcontroller/imidiobject.h>
#include <interfaces/iplugin.h>
#include <interfaces/isoundcontroller/isoundcontroller.h>
#include <interfaces/isoundcontroller/imidievent.h>
#include <plugins/sequencers/alsamidisequencer/alsamidisequencer.h>
#include <plugins/sequencers/alsamidisequencer/alsamidiobject.h>

#include <shell/core.h>

namespace drumstick
{
    class SequencerEvent;
}

namespace Spinet
{
  
K_PLUGIN_FACTORY(MusicsheetGuiFactory, registerPlugin<MusicsheetGui>();)
K_EXPORT_PLUGIN(MusicsheetGuiFactory(KAboutData("musicsheetgui","musicsheetgui",
                                  ki18n("Spinet musicsheet GUI plugin"), "1.0",
                                  ki18n("Spinet musicsheet GUI plugin"),
                                  KAboutData::License_GPL)))

MusicsheetGui::MusicsheetGui(QObject *parent, const QVariantList &args)
: IPlugin(MusicsheetGuiFactory::componentData(), parent)
{
    Q_UNUSED(args);
    
    m_view = new QDeclarativeView;
    m_view->setObjectName("musicsheet");

    connect(ICore::self()->soundController()->midiSequencers()[0],
	    SIGNAL(midiObjectChanged(IMidiObject*)), 
	    SLOT(createNotes(IMidiObject*)));
    
    ICore::self()->uiController()->addDockWidget("Musicsheet", m_view);
}

MusicsheetGui::~MusicsheetGui()
{
}

void MusicsheetGui::unload()
{
}

void MusicsheetGui::createNotes(IMidiObject *midiObject)
{
  kDebug();
  KStandardDirs *k = new KStandardDirs;  
  QList<int> *noteNumbers = new QList<int>;
  
  while(midiObject->hasNext())
  {
    m_midiEvent = midiObject->nextEvent();
    
    if((m_midiEvent->type() == IMidiEvent::SMFNoteOn) && m_midiEvent->param(2) > 0)
    {
      int noteNumber = m_midiEvent->param(1);
      noteNumbers->append(noteNumber);
    }
  }
  
  KeyNotes *keyNotes = new KeyNotes(noteNumbers);
  
  m_view->rootContext()->setContextProperty("keyNotes", keyNotes);
  m_view->setSource(QUrl::fromLocalFile(k->findResource("data", "spinet/MyItem.qml")));
}

}
