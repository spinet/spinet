/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

import QtQuick 1.0

Text {
    function createMusicsheet()
    {
      var componentClef = Qt.createComponent("TrebleClef.qml");
      var clef = componentClef.createObject(texto);
      clef.x = -150;
      clef.y = -225;
      
      var position = -80;
      //var height = -174;
      var height = -174;
      var key = 0;
      
      
      while(keyNotes.hasNext())
      {
	  //console.log("nextNote");
	  key = keyNotes.nextNumber();
	  
	  var component = Qt.createComponent("Note.qml");
	  var note = component.createObject(texto);  
	  note.x = position;
	  note.y = -166-(cleffHeight(key)*6);
	  
	  position += 30;
      }
    }
    
      
    function cleffHeight(noteNumber)
    {
      var number = noteNumber % 12;
      
      switch(number)
      {
	case 0:
	case 1:
	  return 1 + octaveNumber(noteNumber);
	case 2:
	case 3:
	  return 2 + octaveNumber(noteNumber);
	case 4:
	  return 3 + octaveNumber(noteNumber);
	case 5:
	case 6:
	  return 4 + octaveNumber(noteNumber);
	case 7: 
	case 8:
	  return 5 + octaveNumber(noteNumber);
	case 9:
	case 10:
	  return 6 + octaveNumber(noteNumber);
	case 11:
	  return 7 + octaveNumber(noteNumber);
      }
    }
    
    
    function octaveNumber(noteNumber)
    {
      var octave = noteNumber / 12;
      
      console.log(Math.floor(octave));
      
      switch(Math.floor(octave))
      {
	case 0:
	  return -35;
	case 1:
	  return -28;
	case 2:
	  return -21;
	case 3:
	  return -14;
	case 4:
	  return -7;
	case 5:
	  return 0;
	case 6:
	  return 7;
	case 7:
	  return 14;
	case 8:
	  return 21;
	case 9:
	  return 28;
	case 10:
	  return 35;
      }
    }
    

    id: texto
    width: 200
    height: 200
    
    
    Component.onCompleted: createMusicsheet()
}
