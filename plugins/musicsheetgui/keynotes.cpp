/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "keynotes.h"

#include <QtCore/QList> 

namespace Spinet
{


KeyNotes::KeyNotes(QList<int> *noteNumbers, QObject *parent)
: QObject(parent), m_noteNumbers(noteNumbers)
{
    i = m_noteNumbers->begin();
}

KeyNotes::~KeyNotes()
{
}

bool KeyNotes::hasNext()
{    
    if(i != m_noteNumbers->end())
    {
	return true;
    }
    
    return false;
}

int KeyNotes::nextNumber()
{
    int number = *i;
    i++;
    return number;
}

}
