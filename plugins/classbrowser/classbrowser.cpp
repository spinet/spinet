/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/ 

#include "classbrowser.h"

#include <QDebug>

#include <KStandardDirs>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/isoundcontroller/isoundcontroller.h>
#include <interfaces/isoundcontroller/imidisequencer.h>
#include <interfaces/isoundcontroller/imidiobject.h>
#include <interfaces/iplugincontroller.h>
//#include <plugins/midiobjectfactories/smffactory/smffactory.h>

class QWebView;

namespace Spinet
{
   
class SmfFactory;    
class IMidiObject;

K_PLUGIN_FACTORY(ClassBrowserFactory, registerPlugin<ClassBrowser>();)
K_EXPORT_PLUGIN(ClassBrowserFactory(KAboutData("classbrowser","classbrowser",
                                  ki18n("Spinet classbrowser GUI plugin"), "1.0",
                                  ki18n("Spinet classbrowser GUI plugin"),
                                  KAboutData::License_GPL)))

ClassBrowser::ClassBrowser(QObject *parent, const QVariantList &args)
: IPlugin(ClassBrowserFactory::componentData(), parent)
{
    Q_UNUSED(args);
    KStandardDirs *k = new KStandardDirs;  
    m_view = new QWebView;
    m_page = new QWebPage;
    m_view->setObjectName("classBrowser");
    
    m_view->load(QUrl::fromLocalFile(k->findResource("data", "spinet/resources.html")));
    m_view->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    
    connect(m_view, SIGNAL(linkClicked(QUrl)),
	    this, SLOT(getResource(QUrl)));
    
    m_smfFactory = dynamic_cast<ISmfFactory *>(ICore::self()->pluginController()->pluginForExtension("org.liveblue.ISmfFactory", "spinetsmffactory"));
    
    ICore::self()->uiController()->setCentralWidget(m_view);
}


ClassBrowser::~ClassBrowser()
{
}

void ClassBrowser::unload()
{
}

void ClassBrowser::getResource(const QUrl &url)
{
    if(url.scheme() == "spinet")
    {
	KStandardDirs *k = new KStandardDirs;
	QUrl fileDirectory = QUrl::fromLocalFile(k->findResource("data", "spinet/" + url.authority()));

	IMidiObject *midiObject = ICore::self()->soundController()->newMidiObject();

	midiObject = m_smfFactory->fromFile(fileDirectory.toString());
	midiObject->sort();
	midiObject->resetPosition();
	
	ICore::self()->soundController()->midiSequencers()[0]->setMidiObject(midiObject);
    }
    else
    {
	qDebug() << url.toString();
	m_view->load(QUrl(url));
    }
}
  
} 
