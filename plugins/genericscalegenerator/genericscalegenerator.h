/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/ 

#ifndef GENERICSCALEGENERATOR_H
#define GENERICSCALEGENERATOR_H

#include <QHash>
#include <QVariant>
#include <QTreeView>

//#include <interfaces/iplugin.h>

#include "igenericscalegenerator.h"

class QDomNode;

namespace Spinet
{   
    
class GenericScaleGenerator : public IGenericScaleGenerator
{
    Q_OBJECT
    
public:
    explicit GenericScaleGenerator(const KComponentData &instance, QObject *parent, const QVariantList &args);
    virtual ~GenericScaleGenerator();
    
    virtual void unload();
    
    QDomElement exploreNode(QDomNode &node);
    void generate(const QString &scaleName, int initialNote);
    QString printNote(int noteNumber);
    
private:
    QHash<QString, QString> *m_scales;
    QTreeView *m_view;
};
    
}

#endif
