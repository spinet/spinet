/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/ 

#include "genericscalegenerator.h"

#include <QtXml/QDomElement>
#include <QtXml/QDomDocument>

#include <QtCore/QUrl>
#include <QtCore/QFile>
#include <QtCore/QIODevice>

#include <KDE/KDebug>
#include <KDE/KStandardDirs>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>

namespace Spinet
{

/*K_PLUGIN_FACTORY(GenericScaleGeneratorFactory, registerPlugin<GenericScaleGenerator>();)
K_EXPORT_PLUGIN(GenericScaleGeneratorFactory(KAboutData("genericscalegenerator","genericscalegenerator",
                                  ki18n("Spinet genericscalegenerator GUI plugin"), "1.0",
                                  ki18n("Spinet genericscalegenerator GUI plugin"),
                                  KAboutData::License_GPL)))*/

GenericScaleGenerator::GenericScaleGenerator(const KComponentData &instance,
					     QObject *parent, 
					     const QVariantList &args)
: IGenericScaleGenerator(instance, parent)
{
    Q_UNUSED(args);

    m_scales = new QHash<QString, QString>;
    KStandardDirs *k = new KStandardDirs;
    
    m_view = new QTreeView;
    m_view->setObjectName("GenericScaleGenerator");
    
    QDomDocument doc("scalesexample");
    QFile file(k->findResource("data", "spinet/scalesexample.xml"));
    
    if(!file.open(QIODevice::ReadOnly))
      kDebug() << "Erro ao abrir o arquivo.";

    
    if (!doc.setContent(&file)) 
    {
	kDebug() << "Conteudo do arquivo não encontrado.";
	file.close();
    }
    
    file.close();

    
    QDomElement docElements = doc.documentElement();
    QDomNode scales = docElements.firstChild();
    
    while(!scales.isNull())
    {
	QDomElement scalesElement = exploreNode(scales);
	QDomNode scale = scalesElement.firstChild();
	
	while(!scale.isNull())
	{
	    QDomElement scaleName = exploreNode(scale);
	    QDomElement scaleHops = exploreNode(scale);
	    
	    m_scales->insert(scaleName.text(), scaleHops.text());
	    kDebug() << "Inserindo" << scaleName.text() << "e" << scaleHops.text();
	}
    }
    

    generate("minor pentatonic", 0);
}

GenericScaleGenerator::~GenericScaleGenerator()
{
}


QDomElement GenericScaleGenerator::exploreNode(QDomNode &node)
{   
    QDomElement element;
    
    if(!node.isNull())
    {
	element = node.toElement();
	node = node.nextSibling();
	
	return element;
    }   
    
    return element;
}


void GenericScaleGenerator::generate(const QString &scaleName, int note)
{
    QString scaleHops = m_scales->value(scaleName);
    
    kDebug() << printNote(note);
    
    for(int i = 0; i < scaleHops.size(); i++)
    {
	if(scaleHops.at(i) != ',')
	{
	    QString hop = scaleHops.at(i);
	    
	    if((note + hop.toInt()) > 11)
	    {
		note = (note + hop.toInt()) - 12;
	    }
	    else
	    {
		note += hop.toInt();
	    }
	    
	    kDebug() << printNote(note);
	}
    }
}


QString GenericScaleGenerator::printNote(int noteNumber)
{
    switch(noteNumber)
    {
	case 0:
	    return "C";
	case 1:
	    return "C#";
	case 2:
	    return "D";
	case 3:
	    return "D#";
	case 4:
	    return "E";
	case 5:
	    return "F";
	case 6:
	    return "F#";
	case 7:
	    return "G";
	case 8:
	    return "G#";
	case 9:
	    return "A";
	case 10:
	    return "A#";
	case 11:
	    return "B";
	default:
	    break;
    }
    
    return "";
}

void GenericScaleGenerator::unload()
{
}

}


