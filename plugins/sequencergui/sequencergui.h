/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef SEQUENCERGUI_H
#define SEQUENCERGUI_H

#include <QVariant>

#include <interfaces/iplugin.h>

class QWidget;

namespace Ui
{
    class SequencerGui;
}

namespace Spinet
{
  
class IMidiObject;

class SequencerGui : public IPlugin
{
    Q_OBJECT
public:
    explicit SequencerGui(QObject *parent, const QVariantList &args = QVariantList());
    virtual ~SequencerGui();
    virtual void unload();

private Q_SLOTS:
    void on_sldTempo_valueChanged(int value);
    void on_dilVolume_valueChanged(int value);
    void on_sldPosition_valueChanged(int value);

    void midiObjectChanged(IMidiObject *midiObject);
    void midiObjectPlaying();
    void midiObjectStopped();
    
    void tempoFactorChanged(float value);
    void volumeFactorChanged(float value);
    void playingTrackingEvent(int pos, float tempo, int mins, int secs, int cnts);

private:
    Ui::SequencerGui *ui;
    QWidget *m_gui;
};

}

#endif
