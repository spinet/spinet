/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "sequencergui.h"

#include <QtGui/QToolTip>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/isoundcontroller/isoundcontroller.h>
#include <interfaces/isoundcontroller/imidisequencer.h>
#include <interfaces/isoundcontroller/imidiobject.h>

#include <ui_sequencergui.h>

#include <KDE/KDebug>

namespace Spinet
{
  
K_PLUGIN_FACTORY(SequencerGuiFactory, registerPlugin<SequencerGui>();)
K_EXPORT_PLUGIN(SequencerGuiFactory(KAboutData("sequencergui","sequencergui",
                                  ki18n("Spinet sequencer GUI plugin"), "1.0",
                                  ki18n("Spinet sequencer GUI plugin"),
                                  KAboutData::License_GPL)))

SequencerGui::SequencerGui(QObject *parent, const QVariantList &args)
: IPlugin(SequencerGuiFactory::componentData(), parent), ui(new Ui::SequencerGui)
{
    Q_UNUSED(args);
    m_gui = new QWidget;
    m_gui->setObjectName("sequencerGui");
    ui->setupUi(m_gui);
    ICore::self()->uiController()->addDockWidget("MIDI Sequencer", m_gui, Qt::RightDockWidgetArea);
    ui->tbtPause->setIcon(KIcon("spinet-pause"));
    ui->tbtPlay->setIcon(KIcon("spinet-play"));
    ui->tbtStop->setIcon(KIcon("spinet-stop"));
    m_gui->setEnabled(false);

    connect(ui->tbtPlay, SIGNAL(clicked()), ICore::self()->soundController()->midiSequencers()[0], SLOT(play()));
    connect(ui->tbtPause, SIGNAL(toggled(bool)), ICore::self()->soundController()->midiSequencers()[0], SLOT(setPaused(bool)));
    connect(ui->tbtStop, SIGNAL(clicked()), ICore::self()->soundController()->midiSequencers()[0], SLOT(stop()));

    connect(ui->sldTempo, SIGNAL(valueChanged(int)), SLOT(on_sldTempo_valueChanged(int)));
    connect(ui->spbPitch, SIGNAL(valueChanged(int)), ICore::self()->soundController()->midiSequencers()[0], SLOT(setPitchShift(int)));
    connect(ui->dilVolume, SIGNAL(valueChanged(int)), SLOT(on_dilVolume_valueChanged(int)));
    connect(ui->sldPosition, SIGNAL(valueChanged(int)), SLOT(on_sldPosition_valueChanged(int)));

    connect(ICore::self()->soundController()->midiSequencers()[0], SIGNAL(midiObjectChanged(IMidiObject*)), SLOT(midiObjectChanged(IMidiObject*)));
    connect(ICore::self()->soundController()->midiSequencers()[0], SIGNAL(midiObjectPlaying()), SLOT(midiObjectPlaying()));
    connect(ICore::self()->soundController()->midiSequencers()[0], SIGNAL(midiObjectStopped()), SLOT(midiObjectStopped()));
    connect(ICore::self()->soundController()->midiSequencers()[0], SIGNAL(tempoFactorChanged(float)), SLOT(tempoFactorChanged(float)));
    connect(ICore::self()->soundController()->midiSequencers()[0], SIGNAL(volumeFactorChanged(float)), SLOT(volumeFactorChanged(float)));
    connect(ICore::self()->soundController()->midiSequencers()[0], SIGNAL(playingTrackingEvent(int,float,int,int,int)), SLOT(playingTrackingEvent(int,float,int,int,int)));
}

SequencerGui::~SequencerGui()
{
    delete ui;
}

void SequencerGui::unload()
{
}

void SequencerGui::on_sldTempo_valueChanged(int value)
{
    float tempoFactor = (value > 0) ? (value+100)/100.0 : 100.0/(abs(value)+100);
    ICore::self()->soundController()->midiSequencers()[0]->setTempoFactor(tempoFactor);
}

void SequencerGui::on_dilVolume_valueChanged(int value)
{
    float volumeFactor = (value > 0) ? (value+100)/100.0 : 100.0/(abs(value)+100);
    ICore::self()->soundController()->midiSequencers()[0]->setVolumeFactor(volumeFactor);
}

void SequencerGui::on_sldPosition_valueChanged(int value)
{
    ICore::self()->soundController()->midiSequencers()[0]->setPosition(value/100.0);
}

void SequencerGui::midiObjectChanged(IMidiObject *midiObject)
{
    Q_UNUSED(midiObject);
    m_gui->setEnabled(true);
    ui->tbtPlay->setEnabled(true);
    ui->tbtPause->setChecked(false);
    ui->tbtPause->setEnabled(false);
    ui->tbtStop->setEnabled(false);
    ui->sldPosition->setEnabled(false);
}

void SequencerGui::midiObjectPlaying()
{
    ui->tbtPlay->setEnabled(false);
    ui->tbtPause->setEnabled(true);
    ui->tbtStop->setEnabled(true);
    ui->sldPosition->setEnabled(true);
}

void SequencerGui::midiObjectStopped()
{
    ui->tbtPlay->setEnabled(true);
    ui->tbtPause->setChecked(false);
    ui->tbtPause->setEnabled(false);
    ui->tbtStop->setEnabled(false);
    ui->sldPosition->setEnabled(false);
}

void SequencerGui::tempoFactorChanged(float value)
{
    QString tip = QString::number((int)(value*100)) + QString("\%");
    ui->sldTempo->setToolTip(tip);
    QToolTip::showText(QCursor::pos(), tip, ui->sldTempo);
}

void SequencerGui::volumeFactorChanged(float value)
{
    QString tip = QString::number((int)(value*100)) + QString("\%");
    ui->dilVolume->setToolTip(tip);
    QToolTip::showText(QCursor::pos(), tip, ui->dilVolume);
}

void SequencerGui::playingTrackingEvent(int pos, float tempo, int mins, int secs, int cnts)
{
    if (!ui->sldPosition->isSliderDown())
    {
	kDebug() << "CHANGING to " << pos;
	ui->sldPosition->setSliderPosition(pos);
    }

    QString stempo = QString("%1 bpm").arg(tempo, 0, 'f', 2);
    ui->lblTempoValue->setText(stempo);

    static QChar fill('0');
    QString stime = QString("%1:%2.%3").arg(mins, 2, 10, fill)
                                       .arg(secs, 2, 10, fill)
                                       .arg(cnts, 2, 10, fill);
    ui->lblTime->setText(stime);
}

}
