/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef IPLUGIN_H
#define IPLUGIN_H

#include <QtCore/QObject>

#include <kdemacros.h>
#include <KDE/KLocale>
#include <KDE/KAboutData>
#include <KDE/KPluginLoader>
#include <KDE/KXMLGUIClient>
#include <KDE/KPluginFactory>

#include "interfacesexport.h"

#define SPINET_PLUGIN_VERSION 1

namespace Spinet
{

class ICore;

class SPINETINTERFACES_EXPORT IPlugin: public QObject, public KXMLGUIClient
{
    Q_OBJECT
public:
    IPlugin(const KComponentData &instance, QObject *parent);
    virtual ~IPlugin();

    virtual void unload();
    ICore *core() const;
    
    QStringList extensions() const;
    
    template<class Extension> Extension* extension()
    {
        if(extensions().contains( qobject_interface_iid<Extension*>())) 
	{
            return qobject_cast<Extension*>( this );
        }
        return 0;
    }

private:
    class IPluginPrivate *const d;
};

}
#endif
