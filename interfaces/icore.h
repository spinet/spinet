/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef ICORE_H
#define ICORE_H

#include <QtCore/QObject>

#include "interfacesexport.h"

/*! \mainpage This research aims to describe the design and development of an integrated and collaborative opensource environment for teaching fundamentals of music. The Integration and collaboration are characterized respectively by co-ordinated and structured meeting of the tools necessary for education and music creation and the possibility of obtaining online content.
 *
 * \section intro_sec Introduction
 *
 * This is the introduction.
 *
 * \section install_sec Installation
 *
 * \subsection step1 Step 1: Opening the box
 *
 * etc...
 */

namespace Spinet
{

class IUiController;
class IPluginController;
class ISoundController;

class SPINETINTERFACES_EXPORT ICore: public QObject
{
    Q_OBJECT
public:
    virtual ~ICore();
    static ICore *self();
    virtual IUiController     *uiController() = 0;
    virtual IPluginController *pluginController() = 0;
    virtual ISoundController  *soundController() = 0;
    
protected:
    ICore(QObject *parent = 0);
    static ICore *m_self;
};

}

#endif
