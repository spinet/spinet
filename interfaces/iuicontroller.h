/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef IUICONTROLLER_H
#define IUICONTROLLER_H

#include <QtCore/QObject>

#include "interfacesexport.h"

namespace Spinet
{

class SPINETINTERFACES_EXPORT IUiController : public QObject
{
    Q_OBJECT
public:
    IUiController(QObject *parent = 0);
    virtual ~IUiController();

    virtual void setCentralWidget(QWidget *centralWidget) = 0;
    virtual void addDockWidget(const QString &title, QWidget *widget, Qt::DockWidgetArea dockArea = Qt::BottomDockWidgetArea) = 0;
    
    virtual void writeWidgetSettings(QWidget *widget) = 0;
    virtual void readWidgetSettings(QWidget *widget) = 0;
    
Q_SIGNALS:
    void settingsChanged();
};

}

#endif
