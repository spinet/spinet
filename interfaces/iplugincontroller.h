/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef IPLUGINCONTROLLER_H
#define IPLUGINCONTROLLER_H

#include <QtCore/QObject>

#include <KDE/KPluginInfo>

#include <interfaces/iplugin.h>

#include "interfacesexport.h"

namespace Spinet
{

class SPINETINTERFACES_EXPORT IPluginController : public QObject
{
    Q_OBJECT
public:
    IPluginController(QObject *parent = 0);
    virtual ~IPluginController();

    virtual KPluginInfo pluginInfo(const IPlugin *plugin) const = 0;
    virtual QList<IPlugin *> loadedPlugins() const = 0;
    virtual bool unloadPlugin(const QString &pluginName) = 0;
    virtual IPlugin *loadPlugin(const QString &pluginName) = 0;
    
    static KPluginInfo::List query(const QString &serviceType, const QString &constraint);
    static KPluginInfo::List queryPlugins(const QString &constraint);
    
    virtual IPlugin *pluginForExtension(const QString &extension, const QString& pluginName = "" ) = 0;
    
    template<class Extension> Extension* extensionForPlugin(const QString &extension = "", const QString &pluginName = "")
    {
	QString ext;
	
	if(extension.isEmpty())
	{
	    ext = qobject_interface_iid<Extension *>();
	}
	else
	{
	    ext = extension;
	}
	
	IPlugin *plugin = pluginForExtension(ext, pluginName);
	
	if(plugin)
	{
	    return plugin->extension<Extension>();
	}
	
	return 0L;
    }
    
    static KPluginInfo::List queryExtensionPlugins(const QString &extension, const QStringList &constraints = QStringList());

Q_SIGNALS:
    void loadingPlugin(const QString &pluginName);
    void pluginLoaded(IPlugin *plugin, const KPluginInfo &pluginInfo);
    void pluginUnloaded(IPlugin *plugin, const KPluginInfo &pluginInfo);
};

}

#endif
