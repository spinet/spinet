/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "iplugincontroller.h"

#include <KDE/KServiceTypeTrader>

#include "iplugin.h"

namespace Spinet
{

IPluginController::IPluginController(QObject *parent)
: QObject(parent)
{
}

IPluginController::~IPluginController()
{
}

KPluginInfo::List IPluginController::query(const QString &serviceType, const QString &constraint)
{
    KPluginInfo::List infoList;
    KService::List serviceList = KServiceTypeTrader::self()->query(serviceType,
		   QString("%1 and [X-Spinet-Version] == %2").arg(constraint).arg(SPINET_PLUGIN_VERSION));

    infoList = KPluginInfo::fromServices(serviceList);
    return infoList;
}

KPluginInfo::List IPluginController::queryPlugins(const QString &constraint)
{
    return query("Spinet/Plugin", constraint);
}

KPluginInfo::List IPluginController::queryExtensionPlugins(const QString &extension, const QStringList &constraints)
{
    QStringList c = constraints;
    
    c << QString("'%1' in [X-Spinet-Interfaces]").arg(extension);
    
    return queryPlugins(c.join(" and "));
}

}

