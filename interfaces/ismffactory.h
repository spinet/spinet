/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef ISMFFACTORY_H
#define ISMFFACTORY_H

#include <QtCore/QObject>

#include <interfaces/iplugin.h>
#include <interfaces/isoundcontroller/imidiobject.h>

#include "interfacesexport.h"


namespace Spinet
{
    
class IMidiObject;
    
class SPINETINTERFACES_EXPORT ISmfFactory // krazy:exclude=dpointer
{
public:
    explicit ISmfFactory();
    virtual ~ISmfFactory();
    
    virtual IMidiObject *fromFile(const QString &fileName) = 0;
    virtual void unload();
    
private Q_SLOTS:
    virtual void slotOpenMidiFile() = 0;
    
    virtual void slotSMFError(const QString &errorStr) = 0;
    virtual void slotSMFHeader(int format, int ntrks, int division) = 0;
    virtual void slotSMFNoteOn(int chan, int pitch, int vol) = 0;
    virtual void slotSMFNoteOff(int chan, int pitch, int vol) = 0;
    virtual void slotSMFKeyPress(int chan, int pitch, int press) = 0;
    virtual void slotSMFCtlChange(int chan, int ctl, int value) = 0;
    virtual void slotSMFPitchBend(int chan, int value) = 0;
    virtual void slotSMFProgram(int chan, int patch) = 0;
    virtual void slotSMFChanPress(int chan, int press) = 0;
    virtual void slotSMFSysex(const QByteArray &data) = 0;
    virtual void slotSMFSeqSpecific(const QByteArray &data) = 0;
    virtual void slotSMFMetaUnregistered(int typ, const QByteArray &data) = 0;
    virtual void slotSMFMetaMisc(int typ, const QByteArray &data) = 0;
    virtual void slotSMFSequenceNum(int seq) = 0;
    virtual void slotSMFforcedChannel(int channel) = 0;
    virtual void slotSMFforcedPort(int port) = 0;
    virtual void slotSMFText(int typ, const QString &data) = 0;
    virtual void slotSMFSmpte(int b0, int b1, int b2, int b3, int b4) = 0;
    virtual void slotSMFTimeSig(int b0, int b1, int b2, int b3) = 0;
    virtual void slotSMFKeySig(int b0, int b1) = 0;
    virtual void slotSMFTempo(int tempo) = 0;
    virtual void slotSMFendOfTrack() = 0;
    virtual void slotSMFTrackStart() = 0;
    virtual void slotSMFTrackEnd() = 0;
    virtual void slotSMFWriteTempoTrack() = 0;
    virtual void slotSMFWriteTrack(int track) = 0;

};

}

#endif
