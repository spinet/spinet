/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "iplugin.h"

#include "icore.h"
#include "iplugincontroller.h"

namespace Spinet
{

class IPluginPrivate
{
public:
    ICore *core;
    QStringList m_extensions;
};

IPlugin::IPlugin(const KComponentData &instance, QObject *parent)
: QObject(parent),
  KXMLGUIClient(),
  d(new IPluginPrivate)
{
    d->core = static_cast<ICore*>(parent);
    setComponentData(instance);
}

IPlugin::~IPlugin()
{
    delete d;
}

void IPlugin::unload()
{
}

ICore *IPlugin::core() const
{
    return d->core;
}

QStringList Spinet::IPlugin::extensions( ) const
{
  return d->m_extensions;
}

}

