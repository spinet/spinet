/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef IAUDIOSEQUENCER_H
#define IAUDIOSEQUENCER_H

#include "../interfacesexport.h"

#include <interfaces/iplugin.h>

namespace Spinet
{

class SPINETINTERFACES_EXPORT IAudioSequencer : public IPlugin
{
    Q_OBJECT
public:
    IAudioSequencer(const KComponentData &instance, QObject *parent);
    virtual ~IAudioSequencer();

    virtual void play () = 0;
    virtual void setPaused(bool paused) = 0;
    virtual void stop () = 0;
};

}

#endif
