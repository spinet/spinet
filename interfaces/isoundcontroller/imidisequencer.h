/********************************************************************************
 *   Copyright (C) 2010-2011 by Sandro Andrade <sandroandrade@kde.org>          *
 *                 and Luis Paulo Torres de Oliveira <luisoliveira@ifba.edu.br> *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef IMIDISEQUENCER_H
#define IMIDISEQUENCER_H

#include "../interfacesexport.h"
#include "imididevice.h"

namespace Spinet
{

class IMidiObject;

class SPINETINTERFACES_EXPORT IMidiSequencer : public IMidiDevice
{
    Q_OBJECT
public:
    IMidiSequencer(const KComponentData &instance, QObject *parent);
    virtual ~IMidiSequencer();

public Q_SLOTS:
    void setMidiObject(IMidiObject *midiObject);
    void play ();
    void setPaused(bool paused);
    void stop ();
    void setTempoFactor(float value);
    void setVolumeFactor(float value);
    void setPitchShift(int value);
    void setPosition(float value);

protected:
    virtual void setMidiObjectInternal(IMidiObject *midiObject) = 0;
    virtual void playInternal () = 0;
    virtual void setPausedInternal(bool paused) = 0;
    virtual void stopInternal () = 0;
    virtual void setTempoFactorInternal(float value) = 0;
    virtual void setVolumeFactorInternal(float value) = 0;
    virtual void setPitchShiftInternal(int value) = 0;
    virtual void setPositionInternal(float value) = 0;

Q_SIGNALS:
    void midiObjectChanged(IMidiObject *midiObject);
    void midiObjectPlaying();
    void midiObjectPauseToggled(bool paused);
    void midiObjectStopped();
    void tempoFactorChanged(float value);
    void volumeFactorChanged(float value);
    void pitchShiftChanged(int value);
    void positionChanged(float value);
    void playingTrackingEvent(int pos, float tempo, int mins, int secs, int cnts);
};

}

#endif
